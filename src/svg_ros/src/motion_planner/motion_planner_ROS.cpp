/********************************************************************************
*                                                                               *
* motion_planner_ROS.cpp                                                        *
*  ===================                                                          *
*                                                                               *
*  Description:                                                                 *
*  It controls the movement of a robot using state machines.                    *
*  It uses a real robot or a simulated one.                                     *
*                                                                               *
*                               J. Savage                                       *
*                                                                               *
*                               FI-UNAM 2017                                    *
* Modify by:                                                                    *
*   Oropeza Vilchis Luis Alberto                                          *
********************************************************************************/

// System
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <string>

// ROS
#include <ros/console.h>
#include <ros/ros.h>

// Project
#include <state_machines/StateMachine.h>
#include <types/Coord.h>
#include <types/AdvanceAngle.h>
#include <types/Inputs.h>
#include <types/Constants.h>
#include <types/Raw.h>
#include <search_ai/Search.h>
#include <potential_fields/PotentialFields.h>
#include "../simulator/simulation.h"
#include "../utilities/utilities.h"
#include "../ros_utilities/ParserPlanner.h"
#include "../ros_utilities/receiveInputs.h"
#include "../ros_utilities/ros_util.h"
#include "../ros_utilities/over_state.h"


StateMachine configureStateMachine(const Inputs i) {
    StateMachine stateMachine = StateMachine(i.getMagnitudeAdvance(), i.getMaxAngle());
    switch (i.getOptionSelected()) {
        case 1:
            stateMachine.setType(StateMachineType::AVOIDANCE);
            break;
        case 2:
            stateMachine.setType(StateMachineType::DESTINATION);
            break;
        case 3:
        case 6:
        case 12:
            stateMachine.setType(StateMachineType::AVOIDANCE_DESTINATION);
            break;
        case 4:
            stateMachine.setType(StateMachineType::SURROUND_OBSTACLE);
            break;
        case 5:
            stateMachine.setType(StateMachineType::FOLLOW_WALL);
            break;
    }
    return stateMachine;
}

/*
 * It moves the robot from the origin to the destination using state machines
 */
int moveRobot() {
    ParserPlanner parserPlanner = ParserPlanner();
    list<Node> pathNodes;
    Inputs inputs;
    string filenameObservations, stringAuxiliar;
    FILE *fileObservations;
    Raw observations;
    Coord coordRobot, coordDestination, coordSearch;
    bool isDestinationReached;
    bool flagLargestStep = false;
    int numberSteps = 1;
    int quantizedObstacle, quantizedAttraction, quantizedIntensity;
    AdvanceAngle displacement, movVectorPotentialFields, movNodeSearch;
    float largestValue, distance;
    // Flags for environment
    int flagSensor, flagLight, flagBase, flagInvert, flagPlanner;
    // Potential fields
    int numberStates = 0, flagStateMachine = 0;

    ROS_INFO("Waiting data from the GUI");
    inputs = receiveInputs("receiveInputs");
    inputs.setSensorName("laser");
    StateMachine stateMachine = configureStateMachine(inputs);
    PotentialFields potentialFields = PotentialFields(inputs.getD0(), inputs.getEta(), inputs.getD1(),
                                                      inputs.getEpsilon(), inputs.getDelta());
    // TODO: Make configurable path
    string topologicalFile = string("topological_maps/") + inputs.getFileEnvironment() + string(".top");
    filenameObservations = inputs.getPathToFiles() + inputs.getFileEnvironment() + ".raw";
    if ((fileObservations = fopen(filenameObservations.c_str(), "w")) == NULL) {
        ROS_FATAL("[motion_planner_ROS.cpp] File %s can not be open", filenameObservations.c_str());
        return (0);
    }
    stringAuxiliar = "( radio_robot " + to_string(inputs.getRadiusRobot()) + " )\n";
    fputs(stringAuxiliar.c_str(), fileObservations);
    string_to_gui(stringAuxiliar);
    coordRobot.setX(inputs.getXPositionOrigin());
    coordRobot.setY(inputs.getYPositionOrigin());
    coordRobot.setAngle(inputs.getAnglePosition());
    coordDestination.setX(inputs.getXPositionTarget());
    coordDestination.setY(inputs.getYPositionTarget());
    fprintf(fileObservations, "( destination %f %f )\n", inputs.getXPositionTarget(), inputs.getYPositionTarget());
    largestValue = inputs.getLargestSensorValue();
    float K_GOAL = Constants::GOAL_DISTANCE_REACHED * inputs.getMagnitudeAdvance();
    ROS_DEBUG("[motion_planer_ROS.cpp] Selected behaviour: %d", inputs.getOptionSelected());
    ROS_DEBUG("[motion_planer_ROS.cpp] Potential Fields d0: %f | eta: %f | d1: %f | epsilon: %f | delta: %f)",
              inputs.getD0(), inputs.getEta(), inputs.getD1(), inputs.getEpsilon(), inputs.getDelta());
    ROS_DEBUG("[motion_planer_ROS.cpp] Topological map: %s", topologicalFile.c_str());
    ROS_DEBUG("[motion_planer_ROS.cpp] Observations topologicalFile: %s", filenameObservations.c_str());
    ROS_DEBUG("[motion_planer_ROS.cpp] Threshold to reach destination: %f", K_GOAL);

    get_enviroment_flags(&flagSensor, &flagLight, &flagBase, &flagInvert, &flagPlanner);
    if (flagPlanner) {
        ROS_INFO("Planner flag activated");
        parserPlanner.readCommands(string("plan.txt"));
    }

    isDestinationReached = false;
    // It moves the robot to the final destination
    do {
        if (flagPlanner) {
            vector<string> command = parserPlanner.getNextCommnad();
            getchar();
            if (command[0] == "goto") {
                if (command.size() > 2) {
                    coordDestination.setX((float) atof(command[1].substr(1).c_str()));
                    coordDestination.setY((float) atof(command[2].c_str()));
                    isDestinationReached = false;
                    numberSteps = 0;
                    ROS_INFO("[Planner] Changing position (%0.3f, %0.3f) --> (%0.3f, %0.3f)", coordRobot.getX(),
                             coordRobot.getY(), coordDestination.getX(), coordDestination.getY());
                } else {
                    ROS_INFO("[Planner] Moving to %s", command[1].c_str());
                    continue;
                }
            } else if (command[0] == "grasp") {
                ROS_INFO("[Planner] Grasp %s", command[1].c_str());
                continue;
            } else if (command[0] == "release") {
                ROS_INFO("[Planner] Release %s", command[1].c_str());
                continue;
            } else if (command[0] == "pose") {
                coordRobot.setX((float) atof(command[1].substr(1).c_str()));
                coordRobot.setY((float) atof(command[2].c_str()));
                ROS_INFO("[Planner] Initial position %s", command[1].c_str());
                continue;
            } else if (command[0] == "find") {
                ROS_INFO("[Planner] Finding %s", command[1].c_str());
                continue;
            }
        }
        Search search = Search(inputs.getPathToFiles() + topologicalFile, coordRobot, coordDestination);
        switch (inputs.getOptionSelected()) {
            case 7:
                pathNodes = search.dfs();
                break;
            case 8:
                pathNodes = search.bfs();
                break;
            case 9:
                pathNodes = search.ucs();
                break;
            case 10:
                pathNodes = search.gbfs();
                break;
            case 11:
            case 12:
                pathNodes = search.AStar();
                break;
        }

        while (!isDestinationReached) {
            if (flagLight) {
                get_destination_real(&quantizedIntensity, &quantizedAttraction);
            } else {
                get_destination(coordRobot, coordDestination, &quantizedIntensity, &quantizedAttraction);
            }
            distance = calculate_distance(coordRobot, coordDestination);
            if (distance < K_GOAL) {
                ROS_DEBUG("Reached distance destination: %f", distance);
                break;
            }
            // It gets laser observations
            get_observations(coordRobot, observations, largestValue, flagSensor, flagInvert);
            // It send the robot position to the base module
            send_robot_position(coordRobot);
            // It saves the robot's position to be plotted by the GUI
            stringAuxiliar =  "( robot Justina " +  to_string(coordRobot.getX()) + " " +
                    to_string(coordRobot.getY()) + " " + to_string(coordRobot.getAngle()) + " )\n";
            fputs(stringAuxiliar.c_str(), fileObservations);
            string_to_gui(stringAuxiliar);

            // It saves the sensor data to be plot by Python/TK
            write_obs_sensor_pos(fileObservations, observations, inputs.getSensorName().c_str(),
                    inputs.getNumberSensors(), inputs.getAngleSensor(), inputs.getRangeSensor(), coordRobot);
            // It quantizes the real laser inputs ( the real laser has wrong data  in each scan )
            if (flagSensor) {
                quantizedObstacle = quantize_inputs_real(observations, inputs.getNumberSensors(), largestValue);
            } else {
                quantizedObstacle = quantize_inputs(observations, inputs.getNumberSensors());
            }
            ROS_DEBUG("\n\n\t****************** STEP %d ********************", numberSteps);
            ROS_DEBUG("[Pose] (%f, %f, %f)", coordRobot.getX(), coordRobot.getY(), coordRobot.getAngle());
            ROS_DEBUG("[Quantized Intensity] %d", quantizedIntensity);
            ROS_DEBUG("[Quantized Destination] %d", quantizedAttraction);
            ROS_DEBUG("[Destination] (%f, %f)", coordDestination.getX(), coordDestination.getY());
            ROS_DEBUG("[Distance destination] %f ", distance);
            // It executes the desired behaviour
            switch (inputs.getOptionSelected()) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    displacement = stateMachine.next(quantizedObstacle, quantizedAttraction, quantizedIntensity);
                    break;
                case 6:
                    // It calculates the robot's movement using potential fields, if fails then uses an state
                    // machine made that surrounds obstacles
                    displacement = potentialFields.generateNextPosition(coordRobot, coordDestination, inputs,
                            observations, &flagStateMachine);
                    if ((numberStates > 0 && numberStates <= 4) || flagStateMachine) {
                        displacement = stateMachine.next(quantizedObstacle, quantizedAttraction, quantizedIntensity);
                        numberStates++;
                    }
                    if (numberStates > 4) {
                        numberStates = 0;
                        flagStateMachine = 0;
                    }
                    break;
                case 7:
                    // Use DFS algorithm
                    if (!flagLargestStep) {
                        if (!pathNodes.empty()) {
                            coordSearch.setX(pathNodes.front().getX());
                            coordSearch.setY(pathNodes.front().getY());
                            coordSearch.setAngle(0.0f);
                            pathNodes.pop_front();
                            flagLargestStep = calculate_displacement_vector(coordRobot, coordSearch, movNodeSearch,
                                                                            inputs.getMagnitudeAdvance());
                        } else {
                            isDestinationReached = true;
                        }
                    } else {
                        flagLargestStep = calculate_displacement_vector(coordRobot, coordSearch, movNodeSearch,
                                                                        inputs.getMagnitudeAdvance());
                    }
                    ROS_DEBUG("[DFS] (%f, %f)", movNodeSearch.getDistance(), movNodeSearch.getAngle());
                    displacement = movNodeSearch;
                    break;
                case 8:
                    // Use BFS algorithm
                    if (!flagLargestStep) {
                        if (!pathNodes.empty()) {
                            coordSearch.setX(pathNodes.front().getX());
                            coordSearch.setY(pathNodes.front().getY());
                            coordSearch.setAngle(0.0f);
                            pathNodes.pop_front();
                            flagLargestStep = calculate_displacement_vector(coordRobot, coordSearch, movNodeSearch,
                                                                            inputs.getMagnitudeAdvance());
                        } else {
                            isDestinationReached = true;
                        }
                    } else {
                        flagLargestStep = calculate_displacement_vector(coordRobot, coordSearch, movNodeSearch,
                                                                        inputs.getMagnitudeAdvance());
                    }
                    ROS_DEBUG("[BFS] (%f, %f)", movNodeSearch.getDistance(), movNodeSearch.getAngle());
                    displacement = movNodeSearch;
                    break;
                case 9:
                    // Use UCS algorithm
                    if (!flagLargestStep) {
                        if (!pathNodes.empty()) {
                            coordSearch.setX(pathNodes.front().getX());
                            coordSearch.setY(pathNodes.front().getY());
                            coordSearch.setAngle(0.0f);
                            pathNodes.pop_front();
                            flagLargestStep = calculate_displacement_vector(coordRobot, coordSearch, movNodeSearch,
                                                                            inputs.getMagnitudeAdvance());
                        } else {
                            isDestinationReached = true;
                        }
                    } else {
                        flagLargestStep = calculate_displacement_vector(coordRobot, coordSearch, movNodeSearch,
                                                                        inputs.getMagnitudeAdvance());
                    }
                    ROS_DEBUG("[UCS] (%f, %f)", movNodeSearch.getDistance(), movNodeSearch.getAngle());
                    displacement = movNodeSearch;
                    break;
                case 10:
                    // Use GBFS algorithm
                    if (!flagLargestStep) {
                        if (!pathNodes.empty()) {
                            coordSearch.setX(pathNodes.front().getX());
                            coordSearch.setY(pathNodes.front().getY());
                            coordSearch.setAngle(0.0f);
                            pathNodes.pop_front();
                            flagLargestStep = calculate_displacement_vector(coordRobot, coordSearch, movNodeSearch,
                                                                            inputs.getMagnitudeAdvance());
                        } else {
                            isDestinationReached = true;
                        }
                    } else {
                        flagLargestStep = calculate_displacement_vector(coordRobot, coordSearch, movNodeSearch,
                                                                        inputs.getMagnitudeAdvance());
                    }
                    ROS_DEBUG("[GBFS] (%f, %f)", movNodeSearch.getDistance(), movNodeSearch.getAngle());
                    displacement = movNodeSearch;
                    break;
                case 11:
                    // Use A* algorithm
                    if (!flagLargestStep) {
                        if (!pathNodes.empty()) {
                            coordSearch.setX(pathNodes.front().getX());
                            coordSearch.setY(pathNodes.front().getY());
                            coordSearch.setAngle(0.0f);
                            pathNodes.pop_front();
                            flagLargestStep = calculate_displacement_vector(coordRobot, coordSearch, movNodeSearch,
                                                                            inputs.getMagnitudeAdvance());
                        } else {
                            isDestinationReached = true;
                        }
                    } else {
                        flagLargestStep = calculate_displacement_vector(coordRobot, coordSearch, movNodeSearch,
                                                                        inputs.getMagnitudeAdvance());
                    }
                    ROS_DEBUG("[A*] (%f, %f)", movNodeSearch.getDistance(), movNodeSearch.getAngle());
                    displacement = movNodeSearch;
                    break;
                case 12:
                    // Use Potential Fields with A* algorithm
                    if (!flagLargestStep) {
                        if (!pathNodes.empty()) {
                            coordSearch.setX(pathNodes.front().getX());
                            coordSearch.setY(pathNodes.front().getY());
                            coordSearch.setAngle(0.0f);
                            pathNodes.pop_front();
                            flagLargestStep = true;
                        } else {
                            isDestinationReached = true;
                        }
                    }
                    distance = calculate_distance(coordRobot, coordDestination);
                    if (distance < K_GOAL) {
                        flagLargestStep = false;
                        continue;
                    }
                    movVectorPotentialFields = potentialFields.generateNextPosition(coordRobot, coordDestination,
                                                                                    inputs,
                                                                                    observations, &flagStateMachine);
                    if ((numberStates > 0 && numberStates <= 4) || flagStateMachine) {
                        movVectorPotentialFields = stateMachine.next(quantizedObstacle, quantizedAttraction,
                                                                     quantizedIntensity);
                        numberStates++;
                    }
                    if (numberStates > 4) {
                        numberStates = 0;
                        flagStateMachine = 0;
                    }
                    displacement = movVectorPotentialFields;
                    break;
                default:
                    ROS_ERROR("Behaviour %d not defined", inputs.getOptionSelected());
                    isDestinationReached = true;
                    displacement = AdvanceAngle(0.0f, 0.0f);
                    break;
            }
            move_robot(displacement, coordRobot, flagBase);
            write_obs_sensor_pos(fileObservations, observations, inputs.getSensorName().c_str(),
                    inputs.getNumberSensors(), inputs.getAngleSensor(), inputs.getRangeSensor(), coordRobot);
            if (numberSteps > inputs.getNumberSteps()) {
                isDestinationReached = true;
            }
            numberSteps++;
        }
        ROS_DEBUG("Robot position (%f, %f, %f)", coordRobot.getX(), coordRobot.getY(), coordRobot.getAngle());
    } while (flagPlanner && !parserPlanner.isEmpty());
    fclose(fileObservations);
    return numberSteps;
}


int main(int argc, char *argv[]) {
    bool isRunning = true;
    ros::init(argc, argv, "motion_planner_client");
    ROS_INFO("Motion planner node started");
    if (ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Info)) {
        ros::console::notifyLoggerLevelsChanged();
    }
    while (isRunning) {
        moveRobot();
        over(); // Send IS_OVER signal
        usleep(10000);
    }
    return 0;
}
