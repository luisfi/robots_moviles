/*
 * It adds some random noise to sensor inputs, advance distance and angle rotation.
 *
 * @author: Luis Alberto Oropeza Vilchis <luis.oropeza.129@gmail.com
 */

#include <types/Raw.h>
#include <random>

using namespace std;

// TODO: put these values as GUI input fields
const int SENSOR_NOISE_TYPE = 1;
const int ADVANCE_NOISE_TYPE = 0;
const int ANGLE_NOISE_TYPE = 0;

const float ADVANCE_NOISE_UNIFORM_LIM_MIN = -0.004;
const float ADVANCE_NOISE_UNIFORM_LIM_MAX = 0.004;
const float ADVANCE_NOISE_NORMAL_MEAN = 0.003;
const float ADVANCE_NOISE_NORMAL_STDDEV = 0.0003;

const float ANGLE_NOISE_UNIFORM_LIM_MIN = 0.0f;
const float ANGLE_NOISE_UNIFORM_LIM_MAX = 0.03927;
const float ANGLE_NOISE_NORMAL_MEAN = 0.003;
const float ANGLE_NOISE_NORMAL_STDDEV = 0.0003;

const float SENSOR_NOISE_NORMAL_MEAN = 0.003f;
const float SENSOR_NOISE_NORMAL_STDDEV = 0.0003f;
const float SENSOR_NOISE_UNIFORM_LIM_MIN = -0.004;
const float SENSOR_NOISE_UNIFORM_LIM_MAX = 0.004;


void addNoiseToSensorInputs(Raw &sensor_vector, int num_sensors) {
    default_random_engine generator;
    uniform_real_distribution<float> uniform(SENSOR_NOISE_UNIFORM_LIM_MIN, SENSOR_NOISE_UNIFORM_LIM_MAX);
    normal_distribution<float> normal(SENSOR_NOISE_NORMAL_MEAN, SENSOR_NOISE_NORMAL_STDDEV);
    float noise;

    for (int k = 0; k < num_sensors; k++) {
        if (SENSOR_NOISE_TYPE == 1) {
            noise = normal(generator);
        } else {
            noise = uniform(generator);
        }
        sensor_vector.setSensors(k, sensor_vector.getSensors(k) + noise);
    }
}

void getRandomAdvanceAngle(float *advance, float *angle) {
    default_random_engine generator;

    if (ADVANCE_NOISE_TYPE == 1) {
        normal_distribution<float> normal(ADVANCE_NOISE_NORMAL_MEAN, ADVANCE_NOISE_NORMAL_STDDEV);
        *advance = normal(generator);
    } else {
        uniform_real_distribution<float> uniform(ADVANCE_NOISE_UNIFORM_LIM_MIN, ADVANCE_NOISE_UNIFORM_LIM_MAX);
        *advance = uniform(generator);
    }
    if (ANGLE_NOISE_TYPE == 1) {
        normal_distribution<float> normal(ANGLE_NOISE_NORMAL_MEAN, ANGLE_NOISE_NORMAL_STDDEV);
        *angle = normal(generator);
    } else {
        uniform_real_distribution<float> uniform(ANGLE_NOISE_UNIFORM_LIM_MIN, ANGLE_NOISE_UNIFORM_LIM_MAX);
        *angle = uniform(generator);
    }
}