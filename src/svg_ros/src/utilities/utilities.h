/****************************************************
 * utilities.h                                      *
 * Hecho por:                                       *
 * @author: J. Savage                               *
 *                                                  *
 * Modificado por:                                  *
 * @author: Luis A. Oropeza V.                      *
 ****************************************************/

#include "svg_ros/LightSrv.h"
#include "svg_ros/MVServ.h"
#include "svg_ros/SensorSrv.h"
#include "svg_ros/ReadySrv.h"
#include "svg_ros/LightRealSrv.h"
#include "svg_ros/EnvironmentSrv.h"

#include <types/Coord.h>
#include <types/Behaviour.h>
#include <types/AdvanceAngle.h>
#include <types/Constants.h>
#include <types/Raw.h>
#include <vector>
#include <string>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

using namespace std;

int ready_gui = 0;

/*
 * string to values
 */
int get_move_params(std::string str, float *a, float *b) {
    using namespace boost::algorithm;
    std::vector<std::string> tokens;

    split(tokens, str, is_any_of(" ")); // here it is
    *a = atof(tokens.at(1).c_str());
    *b = atof(tokens.at(2).c_str());

    return 0;
}

/*
 * Sends string s to GUI
 */
int string_to_gui(string s) {
    static int flg = 1;
    static ros::NodeHandle n;
    static ros::ServiceClient client;
    static svg_ros::MVServ srv;


    if (flg == 1) {
        client = n.serviceClient<svg_ros::MVServ>("mv_gui_comm");
        flg = 0;
    }
    srv.request.param = s;
    if (client.call(srv)) {
        //ROS_DEBUG_NAMED("client", "success to call service: mv_gui_comm");
        return 0;
    } else {
        ROS_ERROR("Failed to call service mv_gui_comm");
        return 1;
    }
}

// TODO: ver que hace la función
//unused at the moment:
// int  get_mv_params2(std::string str,float *a, float* b){
//   float x,y;
//   char * cstr = new char [str.length()+1];
//   std::strcpy (cstr, str.c_str());
//   // cstr now contains a c-string copy of str
//   char * p = std::strtok (cstr," ");
//   int i=0;
//   while (p!=0 && i<=2)
//   {
//     //std::cout << p << '\n';
//     p = std::strtok(NULL," ");
//     if(i==1){
//       x=atof(p);
//     }
//     else if(i==2){
//       y=atof(p);
//     }
//     i++;
//   }
//   delete[] cstr;
//   *a=x;
//   *b=y;
//   return 0;
// }


/*
 * Communication with robot's base
 */
int ROS_move_simulator(std::string s, float *answer_distance, float *answer_angle) {
    ros::NodeHandle n;
    ros::ServiceClient client = n.serviceClient<svg_ros::MVServ>("move_robot");
    svg_ros::MVServ srv;

    srv.request.param = s;
    if (client.call(srv)) {
        //ROS_DEBUG_NAMED("client", "success to call service: move_robot");
    } else {
        ROS_ERROR("Failed to call service move_robot");
        return 1;
    }
    get_move_params(srv.response.answer.c_str(), answer_distance, answer_angle);

    return 0;
}

int ROS_move_turtle(std::string s, float *answer_distance, float *answer_angle) {
    ros::NodeHandle n;
    ros::ServiceClient client = n.serviceClient<svg_ros::MVServ>("move_robot_turtle");
    svg_ros::MVServ srv;

    srv.request.param = s;
    if (client.call(srv)) {
        // ROS_DEBUG_NAMED("client", "success to call service: move_robot_turtle");
    } else {
        ROS_ERROR("Failed to call service move_robot");
        return 1;
    }
    get_move_params(srv.response.answer.c_str(), answer_distance, answer_angle);

    return 0;

}


int get_enviroment_flags(int *sensorflag, int *lightflag, int *baseflag, int *invertflag, int *plannerflag) {
    static ros::NodeHandle n;
    static ros::ServiceClient client;
    static svg_ros::EnvironmentSrv srv;

    client = n.serviceClient<svg_ros::EnvironmentSrv>("environment");
    if (client.call(srv)) {
        //ROS_DEBUG_NAMED("client", "success to call service: environment");
        ROS_DEBUG("Flags:\n \t Sensor:%d  Light:%d   Base:%d   Invert: %d", srv.response.sensor, srv.response.light,
                  srv.response.base, srv.response.invert);
        *sensorflag = srv.response.sensor;
        *lightflag = srv.response.light;
        *baseflag = srv.response.base;
        *invertflag = srv.response.invert;
        *plannerflag = srv.response.planner;
    } else {
        ROS_ERROR("Failed to call service: send_position");
        return 1;
    }

    return 1;
}

/*
 * It gets data from the simulator or the real robot
 */
int get_observations(Coord robot, Raw &observations, float largestValue, int flag_sensor, int flag_inverted) {
    static ros::NodeHandle n;
    static ros::ServiceClient client;
    static svg_ros::SensorSrv srv;
    int j = 0, i = 0;

    srv.request.xPosition = robot.getX();
    srv.request.yPosition = robot.getY();
    srv.request.anglePosition = robot.getAngle();
    if (flag_sensor) {
        // Real
        client = n.serviceClient<svg_ros::SensorSrv>("send_position");
    } else {
        // Simulated
        client = n.serviceClient<svg_ros::SensorSrv>("send_position2");
    }
    if (!client.call(srv)) {
        ROS_ERROR("Failed to call service send_position");
        return 1;
    }
    observations.setFlag(srv.response.flag);
    observations.setRegion(srv.response.region);
    observations.setX(srv.response.x);
    observations.setY(srv.response.y);
    observations.setTheta(srv.response.theta);
    if (flag_sensor && flag_inverted) {
        for (j = 0, i = srv.response.numberSensors - 1; j < srv.response.numberSensors; j++, i--) {
            if (srv.response.sensors[j] > largestValue) {
                observations.setSensors(i, largestValue);
            } else {
                observations.setSensors(i, srv.response.sensors[j]);
            }
            ROS_DEBUG("observ->sensors[%d] %f response.sensors[%d] %f", j, observations.getSensors(j), j,
                      srv.response.sensors[j]);
        }
    } else {
        for (j = 0; j < srv.response.numberSensors; j++) {
            if (srv.response.sensors[j] > largestValue) {
                observations.setSensors(j, largestValue);
            } else {
                observations.setSensors(j, srv.response.sensors[j]);
            }
            ROS_DEBUG("observ->sensors[%d]: %f response.sensors[%d]: %f", j, observations.getSensors(j), j,
                      srv.response.sensors[j]);
        }
    }
    return 0;
}

/*
 * It send the robot position to the base module either the simulator or the real robot
 */
int send_robot_position(Coord robot) {

    static ros::NodeHandle n;
    static ros::ServiceClient client;
    static svg_ros::MVServ srv;
    static int flg = 1;

    srv.request.coord_x = robot.getX();
    srv.request.coord_y = robot.getY();
    srv.request.coord_ang = robot.getAngle();
    if (flg == 1) {
        client = n.serviceClient<svg_ros::MVServ>("send_position_mv");
        flg = 0;
    }
    if (client.call(srv)) {
        //ROS_DEBUG_NAMED("client", "success to call service: send_position_mv");
    } else {
        ROS_ERROR("Failed to call service send_position");
        return 1;
    }

    return 0;
}


/*
 * Gets destination Coordinates and quantized values via ROS communication with light_node
 */
int
get_destination(Coord robot, Coord destination, int *quantized_intensity = NULL, int *quantized_attraction = NULL) {
    static int flg = 1;
    static ros::NodeHandle n;
    static ros::ServiceClient client;
    static svg_ros::LightSrv srv;

    srv.request.req = 1;
    srv.request.coord_x = robot.getX();
    srv.request.coord_y = robot.getY();
    srv.request.coord_ang = robot.getAngle();
    if (flg == 1) {
        client = n.serviceClient<svg_ros::LightSrv>("send_destination");
        flg = 0;
    }
    if (client.call(srv)) {
        //ROS_DEBUG_NAMED("client", "success to call service: send_destination");
    } else {
        ROS_ERROR("Failed to call service send_destination");
        return 1;
    }
    // Either get dest Coords or quantized values
    /*if(srv.response.flag_dest){
      Coord_dest->xc=srv.response.x;
      Coord_dest->yc=srv.response.y;
    }else{*/
    *quantized_intensity = srv.response.quantized_intensity;
    *quantized_attraction = srv.response.quantized_attraction;
    //}

    //printf("Answer get_destination light sensor: %d %d\n", srv.response.quantized_attraction,srv.response.quantized_intensity);

    return 0;
}


int get_destination_real(int *quantized_intensity = NULL, int *quantized_attraction = NULL) {
    static int flg = 1;
    static ros::NodeHandle n;
    static ros::ServiceClient client;
    static svg_ros::LightRealSrv srv;

    srv.request.req = 1;
    if (flg == 1) {
        client = n.serviceClient<svg_ros::LightRealSrv>("Light_Srv");
        flg = 0;
    }
    if (client.call(srv)) {
        //ROS_DEBUG_NAMED("client", "success to call service: Light_Srv");
    } else {
        ROS_ERROR("Failed to call service LightSrv");
        return 1;
    }
    *quantized_intensity = srv.response.quantized_intensity;
    *quantized_attraction = srv.response.quantized_attraction;

    return 0;

}

/*
 * It writes the sensors' readings to be plot by Tk
 */
int write_obs_sensor(FILE *fpw, Raw observations, char *sensor, int num_sensors, float start_angle, float range) {
    int j;
    char aux[250], aux1[4], aux2[20];

    sprintf(aux, "( sensor %s %d %f %f", sensor, num_sensors, range, start_angle);
    fputs(aux, fpw);
    for (j = 0; j < num_sensors; j++) {
        sprintf(aux2, " %f", observations.getSensors(j));
        fputs(aux2, fpw);
        strcat(aux, aux2);
        ROS_DEBUG("[utilities.h - write_obs_sensor] %f", observations.getSensors(j));
    }
    sprintf(aux1, " )\n");
    fputs(aux1, fpw);
    strcat(aux, aux1);
    //send plotting data to gui

    string_to_gui(aux);

    return (1);
}


/*
 * It writes the sensors readings and position to be plot by Tk
 */
int write_obs_sensor_pos(FILE *f, Raw observations, char const*sensor, int num_sensors, float start_angle, float range,
                         Coord robot) {
    int j = 0;
    // TODO: desperdiciar más memoria, por qué sólo 2000
    char aux[2000], aux1[4];

    sprintf(aux, "( sensor %s %f %f %f %d %f %f", sensor, robot.getX(), robot.getY(), robot.getAngle(),
            num_sensors, range, start_angle);
    for (j = 0; j < num_sensors; j++) {
        char aux2[20];
        sprintf(aux2, " %f", observations.getSensors(j));
        strcat(aux, aux2);
        //ROS_DEBUG(" %f",observations.sensors[j]);
    }
    sprintf(aux1, " )\n");
    strcat(aux, aux1);
    fputs(aux, f);
    //send plotting data to gui
    string_to_gui(aux);

    return 1;
}

/*
 * Calcula la magnitud de un vector
 */
float vector_magnitud(Coord v) {
  Coord result = v * v;
  return (float) sqrt(result.getX() + result.getY());
}

/*
 * Calcula la diferencia entre dos vectores
 */
Coord dif_vectors(Coord v1, Coord v2) {
    return v1-v2;
}


// TODO: Checar qué hace la función
/*
 // It quantizes the intensity 
int quantize_intensity(Coord Coord_robot,Coord coor_destination){

 int value=0;
 Coord attraction_force;
 float mag;

 attraction_force=dif_vectors(Coord_robot,coor_destination);
 mag=vector_magnitude(attraction_force);

 if(mag > K_INTENSITY){
        value = 0;
 }
 else{
         value = 1;
 }

 return value;
}
*/


/*
 * It gets the range from the laser sensors
 */
void get_average_sensor(Raw observations, int start, int end, float *average) {
    int i;
    float sd = 0;

    for (i = start; i < end; i++) {
        sd = sd + observations.getSensors(i);
    }
    *average = sd / (float) (end - start);
}

/*
 * It quantizes the inputs
 */
int quantize_inputs(Raw observations, int size) {
    int value = 0, i = 0, interval = 0;
    float left = 0.0f, right = 0.0f;

    interval = size / 2;
    get_average_sensor(observations, interval, size, &left);
    //ROS_DEBUG("left sensor %f\n",left);
    get_average_sensor(observations, 0, interval, &right);
    //ROS_DEBUG("right sensor %f\n",right);
    if (left < Constants::SENSOR_THRESHOLD) {
        value = (value << 1) + 1;
    } else {
        value = (value << 1) + 0;
    }
    if (right < Constants::SENSOR_THRESHOLD) {
        value = (value << 1) + 1;
    } else {
        value = (value << 1) + 0;
    }
    value = value & 0xFFFFFFFF;

    return value;
}

void get_average_sensor_real(Raw observations, int start, int end, float *average, float largest_value) {
    int i = 0;
    float sd = 0;
    float distancia = largest_value;
    float bad_lecture = 0;

    for (i = start; i < end; i++) {
        if (observations.getSensors(i) >= distancia) {
            sd = sd + distancia;
        } else if (observations.getSensors(i) == 0) { // Checks if a value is  NaN
            bad_lecture++;
        } else {
            sd = sd + observations.getSensors(i);
        }
    }
    *average = sd / (((float) (end - start)) - bad_lecture);
}


/*
 * It quantizes the inputs
 */
int quantize_inputs_real(Raw observations, int size, float largest_value) {
    int value = 0, i = 0, interval = 0;
    float left = 0.0f, right = 0.0f;

    interval = size / 2;
    get_average_sensor_real(observations, interval, size, &left, largest_value);
    //ROS_DEBUG("left sensor %f\n",left);
    get_average_sensor_real(observations, 0, interval, &right, largest_value);
    //ROS_DEBUG("right sensor %f\n",right);
    //if( left < Constants::SENSOR_THRESHOLD) value = (value << 1) + 1;
    if ((left * 100 / largest_value) <= 98) {
        value = (value << 1) + 1;
    } else {
        value = (value << 1) + 0;
    }
    // if( right < Constants::SENSOR_THRESHOLD) value = (value << 1) + 1;
    if ((right * 100 / largest_value) <= 98) {
        value = (value << 1) + 1;
    } else {
        value = (value << 1) + 0;
    }
    //ROS_DEBUG("value decimal %d\n",value);
    value = value & 0xFFFFFFFF;
    //ROS_DEBUG("value hexadecimal%x\n",value);

    return value;
}


/*
 * This function is used to calculate the rotation angle for the Mvto command
 */
float get_angle(float ang, float c, float d, float X, float Y) {
    float x, y;

    x = c - X;
    y = d - Y;
    if ((x == 0) && (y == 0)) {
        return (0);
    }
    if (fabs(x) < 0.0001) {
        return ((float) ((y < 0.0f) ? 3 * Constants::PI / 2 : Constants::PI / 2) - ang);
    } else {
        if (x >= 0.0f && y >= 0.0f) {
            return ((float) (atan(y / x) - ang));
        } else if (x < 0.0f && y >= 0.0f) {
            return ((float) (atan(y / x) + Constants::PI - ang));
        } else if (x < 0.0f && y < 0.0f) {
            return ((float) (atan(y / x) + Constants::PI - ang));
        } else {
            return ((float) (atan(y / x) + 2 * Constants::PI - ang));
        }
    }
}

/*
 * Función que divide un vector v sobre el escalar s
 */
Coord divide_vector_scalar(Coord v, float s) {
    Coord div;

    div.setX(v.getX() / s);
    div.setY(v.getY() / s);

    return div;
}


/*
 * It gets an unit vector
 */
Coord get_unit_vector(Behaviour Bvector) {
    float mag;
    Coord unit_vector = Coord(0.0, 0.0, 0.0);
    Coord vector;
    vector.setX(Bvector.getX());
    vector.setY(Bvector.getY());
    mag = vector_magnitud(vector);
    if (mag != 0) {
        unit_vector = divide_vector_scalar(vector, mag);
    }

    return unit_vector;
}


/*
 * It will move the robot the desire angle and distance
 */
void move_robot(AdvanceAngle dist_theta, Coord &robot, int flag_environment) {
    int flg = 0;
    float x, y, theta;
    float new_x, new_y, new_theta;
    float xc, yc;
    float speed;
    float distance, angle;
    float answer_distance;
    float answer_angle;
    string param;

    x = robot.getX();
    y = robot.getY();
    theta = robot.getAngle();
    angle = dist_theta.getAngle();
    distance = dist_theta.getDistance();
    if (angle > 5.75f) {
        angle = -(angle - 5.75f);
    }
    param = "mv " + to_string(distance) + " " + to_string(angle);
    if (flag_environment) {
        ROS_move_turtle(param, &answer_distance, &answer_angle);
    } else {
        ROS_move_simulator(param, &answer_distance, &answer_angle);
    }
    angle = answer_angle;
    distance = answer_distance;
    new_theta = theta + angle;
    // Normaliza el valor del ángulo en el rango [0, 360]
    if (new_theta > 6.2832) {
        new_theta = new_theta - (float) 6.2832;
    } else if (new_theta < 0.0) {
        new_theta = new_theta + (float) 6.2832;
    }
    new_x = x + distance * (float) cos(new_theta);
    new_y = y + distance * (float) sin(new_theta);

    // TODO: validar que el robot no este dentro del obstáculo
    // It checks if the robot new position is inside an obstacle
    //flg = checkNewPosition(xmv,ymv,new_xmv,new_ymv,.5);

    //if(flg==0){
    robot.setX(new_x);
    robot.setY(new_y);
    robot.setAngle(new_theta);
    //}
    //else{
    //printf("The robot remains with the previous position\n");
    //}
    //printf("mv x:%f, y:%f,  rad:%f\n",Coord_robot->xc,Coord_robot->yc,Coord_robot->anglec);
}


/*
 * Calcula la  distancia entre dos Coordenadas
 */
float calculate_distance(Coord v1, Coord v2) {
  Coord distance = v1 - v2;
  distance = distance * distance;

  return (float) sqrt( distance.getX() + distance.getY());
}


/*
 * It generates a robot's output
 */
AdvanceAngle generate_output(int out, float advance, float angle) {
    AdvanceAngle output;

    switch (out) {
        case 0: // Stop
            output.setDistance( 0.0f);
            output.setAngle( 0.0f);
            //ROS_DEBUG("STOP\n");
            break;
        case 1: // Forward
            output.setDistance( advance);
            output.setAngle( 0.0f);
            //ROS_DEBUG("FORWARD\n");
            break;
        case 2: // Backward
            output.setDistance( -advance);
            output.setAngle( 0.0f);
            //ROS_DEBUG("BACKWARD\n");
            break;
        case 3:// Turn left
            output.setDistance( 0.0f);
            output.setAngle( angle);
            //ROS_DEBUG("LEFT\n");
            break;
        case 4: // Turn right
            output.setDistance( 0.0f);
            output.setAngle( -angle);
            //ROS_DEBUG("RIGHT\n");
            break;
        default:
            ROS_INFO("[Utilities.h - generate_output] Output %d not defined is used", out);
            output.setDistance( 0.0f);
            output.setAngle( 0.0f);
            break;
    }
    return output;
}

bool compareFloat(float f1, float f2) {
    float resta = std::abs(f1 - f2);
    if (resta < 0.000000001) {
        return true;
    } else {
        return false;
    }
}

bool calculate_displacement_vector(Coord robot, Coord next_pos, AdvanceAngle &r, float maxDistance) {
    Coord diff = next_pos - robot;

    r.setDistance(vector_magnitud(diff));
    r.setAngle(std::atan2(diff.getY(), diff.getX()) - robot.getAngle());
    if (r.getDistance() > maxDistance) {
        r.setDistance(maxDistance);
        return true;
    }
    return false;
}
