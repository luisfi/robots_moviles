/*
 * @author Oropeza Vilchis Luis Alberto (luisunamfi@gmail.com)
 */

#include <ros/console.h>
#include <search_ai/Connection.h>

/************************************************
 *                Constructors                  *
 ************************************************/
Connection::Connection(Node *n, float cost) {
    this->n = n;
    this->cost = cost;
}


/************************************************
*                    Setters                   *
************************************************/
void Connection::setNode(Node *n) {
    this->n = n;
}

void Connection::setCost(float c) {
    this->cost = c;
}

/************************************************
 *                    Getters                   *
 ************************************************/
Node* Connection::getNode() {
    return this->n;
}

float Connection::getCost() {
    return  this->cost;
}

/************************************************
 *                   Overloads                  *
 ************************************************/
bool Connection::operator ==(const Connection &c) const{
    return (this->n == c.n) && (this->cost == c.cost);
}

bool Connection::operator <(const Connection &n) const{
    return this->cost > n.cost;
}

bool Connection::operator >(const Connection &n) const{
    return this->cost < n.cost;
}

bool Connection::operator <=(const Connection &n) const{
    return this->cost <= n.cost;
}

bool Connection::operator >=(const Connection &n) const{
    return this->cost >= n.cost;
}

/************************************************
 *                    Methods                   *
 ************************************************/
void Connection::printData() {
    ROS_DEBUG("[Connection.cpp] Cost: %f", cost);
}

void Connection::addHeuristic(float h) {
    this->cost += h;
}


