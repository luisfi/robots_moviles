/*
 * @author Oropeza Vilchis Luis Alberto (luisunamfi@gmail.com)
 */

#include <ros/ros.h>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <cstring>
#include <string>
#include <fstream>
#include <vector>
#include <list>
#include <search_ai/ParserNodes.h>
#include <search_ai/Node.h>

using namespace std;

/************************************************
 *                Constructors                  *
 ************************************************/
ParserNodes::ParserNodes() {
    this->numberNodes = 0;
    this->fileName = "";
}

ParserNodes::ParserNodes(string filename) {
    this->numberNodes = 0;
    this->fileName = filename;
    parse();
}

/************************************************
 *                    Setters                   *
 ************************************************/
void ParserNodes::setNumberNodes(int numNodes) {
    this->numberNodes = numNodes;
}

void ParserNodes::setFileName(string fileName) {
    this->fileName = fileName;
    parse();
}

/************************************************
 *                    Getters                   *
 ************************************************/
Node* ParserNodes::getNodes() {
    return nodes;
}

int ParserNodes::getNumberNodes() {
    return this->numberNodes;
}

string ParserNodes::getFileName() {
    return this->fileName;
}

/************************************************
 *                    Methods                   *
 ************************************************/

/**
 * Obtiene los datos de los nodos y las conexiones entre ellos
 */
void ParserNodes::parse() {
    ifstream fileNodes;
    vector<string> results;
    fileNodes.open(this->fileName.c_str());
    if (!fileNodes) {
        printf("[ERROR] No se pudo abrir el archivo %s", fileName.c_str());
    }
    // TODO: verficar que no sea una línea en blanco
    for (string s; getline(fileNodes, s); ) {
        boost::split(results, s,  boost::is_any_of(" "));
        if (results.at(1) == string("node")) {
            createNode(results.at(2), (float)atof(results.at(3).c_str()), (float)atof(results.at(4).c_str()));
        } else if (results.at(1) == string("connection")) {
            addConnectionNode(results.at(2), results.at(3), (float)atof(results.at(4).c_str()));
        }
    }
}

/**
 * Crea un objeto de la clase Nodo y lo almacena
 * @param name nombre del nodo
 * @param x coordenada x
 * @param y coordenada y
 */
void ParserNodes::createNode(string name, float x, float y) {
    Node n(name, x, y);
    n.setParent(nullptr);
    this->nodes[numberNodes] = n;
    numberNodes++;
}

/**
 * Añade una conexión a un nodo
 * @param nameNode nombre del nodo
 * @param nameDestination nombre del nodo al que se conecta
 * @param distance distancia entre los nodos
 */
void ParserNodes::addConnectionNode(string nameNode, string nameDestination, float distance) {
    int index = searchNode(nameNode);
    if (index >= 0) {
        // printf("Sí elemento %s hacia %s con %f\n", nameNode.c_str(), nameDestination.c_str(), distance);
        nodes[index].addConnection(nameDestination, distance);
    } else {
        printf("No se encontró ese elemento %s\n", nameNode.c_str());
    }

}

/**
 * Muestra la información de los nodos almacenados
 */
void ParserNodes::printNodes() {
    for(int i=0; i < this->numberNodes; i++) {
        nodes[i].printData();
    }
    printf("Numero total de nodos %d\n", this->numberNodes);
}

int ParserNodes::searchNode(string name) {
    for (int i=0; i < this->numberNodes; i++) {
        if (nodes[i].getName() == name) {
            return i;
        }
    }
    return -1;
}

/**
 * Devuelve los nodos a los que se conecta un nodo
 * @param node índice del nodo del que se quieren obtener las conexiones
 * @return lista de nodos a
 */
list<Node> ParserNodes::getConnectionsOfNode(int node) {
    return nodes[node].getNodeConnections(this->nodes, this->numberNodes);
}

/**
 * Imprime las conexiones que tiene un nodo
 * @param node indice del nodo
 */
void ParserNodes::printConnectionsOfNode(int node) {
    list<Node> c = getConnectionsOfNode(node);
    printf("-------------Connections of node: %s\n", nodes[node].getName().c_str());
    for (auto & n: c) {
        n.printData();
    }
}

/**
 * Imprime las conexiones que tiene un nodo
 * @param node nombre del nodo
 */
void ParserNodes::printConnectionsOfNode(string node) {
    int index = searchNode(node);
    printConnectionsOfNode(index);
}

/**
 * Devuelve los nodos a los cual se conecta un nodo
 * @param node nombre del nodo
 * @return lista de nodos a los que se conecta
 */
list<Node> ParserNodes::getConnectionsOfNode(string node) {
    int index = searchNode(node);
    return getConnectionsOfNode(index);
}

/**
 * Busca un nodo por su nombre y lo devuelve
 * @param name nombre del nodo
 * @return objeto de la clase Node
 */
Node ParserNodes::getNode(string name) {
    int index = searchNode(name);
    return getNode(index);
}

/**
 * Busca un nodo por su índice y lo devuelve
 * @param name nombre del nodo
 * @return objeto de la clase Node
 */
Node ParserNodes::getNode(int index) {
    if (index >= 0) {
        return nodes[index];
    }
    return Node("error", -1.0f, -1.0f);
}

/**
 * Busca el nodo mas cercano a n
 * @param n nodo
 * @return nodo  más cercano
 */
Node ParserNodes::getClosestNode(Node n) {
    float value = 0.0f, lastValue = 0xFFFFFFFFF;
    int index = -1;
    for (int i = 0; i < this->numberNodes; ++i) {
        value = n.getDistance(this->nodes[i]);
        if (n == this->nodes[i]) {
            continue;
        }
        if (value < lastValue) {
            index = i;
            lastValue = value;
        }
    }
    return getNode(index);
}

/**
 * Conecta el nodo del robot al más cercano que encuentre
 */
void ParserNodes::createConnectionRobot() {
    Node robot = getNode("robot");
    Node closest = getClosestNode(robot);
    ROS_DEBUG("Se conecta al robot con el siguiente nodo:");
    closest.printData();
    addConnectionNode(robot.getName(), closest.getName(), robot.getDistance(closest));
}

/**
 * Conecta el nodo del destino al más cercano que encuentre
 */
void ParserNodes::createConnectionDestination() {
    Node dest = getNode("destination");
    Node closest = getClosestNode(dest);
    ROS_DEBUG("Se conecta al destino con el nodo\n");
    closest.printData();
    addConnectionNode(closest.getName(), "destination", dest.getDistance(closest));
}

/**
 * Añade el nodo de la posición del robot
 * @param x coordenada x
 * @param y coordenada y
 */
void ParserNodes::addRobot(float x, float y) {
    createNode("robot", x, y);
    createConnectionRobot();
}

/**
 * Añade el nodo de la posición del destino
 * @param x coordenada x
 * @param y coordenada y
 */
void ParserNodes::addDestination(float x, float y) {
    createNode("destination", x, y);
    createConnectionDestination();
}