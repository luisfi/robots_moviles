/*
 * @author Oropeza Vilchis Luis Alberto (luisunamfi@gmail.com)
 */

#include <ros/console.h>
#include <search_ai/Node.h>

/************************************************
 *                Constructors                  *
 ************************************************/
Node::Node() {
    this->name = "";
    this->x = 0.0f;
    this->y = 0.0f;
    this->parent = nullptr;
}

Node::Node(string name, float x, float y){
    this->name = name;
    this->x  = x;
    this->y = y;
    this->parent = nullptr;
}

Node::Node(string name, float x, float y, Node *parent, map<string, float> connections) {
    this->name = name;
    this->x = x;
    this->y = y;
    this->parent = parent;
    this->connections = connections;
}

Node::Node(string name, float x, float y, Node *parent){
    this->name = name;
    this->x = x;
    this->y = y;
    this->parent = parent;
}


/************************************************
 *                    Setters                   *
 ************************************************/

void Node::setX(float x) {
    this->x = x;
}

void Node::setY(float y) {
    this->y = y;
}

void Node::setName(string name) {
    this->name = name;
}

void Node::setPosition(float x, float y) {
    this->x = x;
    this->y = y;
}

void Node::setParent(Node *p) {
    this->parent = p;
}


/************************************************
 *                    Getters                   *
 ************************************************/

float Node::getX() {
    return this->x;
}

float Node::getY() {
    return this->y;
}

string Node::getName() {
    return this->name;
}

map<string, float> Node::getMapConnections() {
    return this->connections;
};

list<Node> Node::getNodeConnections(Node nodos[], int numNodos) {
    list<Node> c;
    for (auto const& n: connections) {
        for (int i = 0; i < numNodos; ++i) {
            if (nodos[i].getName() == n.first) {
                Node aux = nodos[i];
                aux.setParent(this);
                c.push_front(aux);
            }
        }
    }
    return c;
}

list<Connection> Node::getConnections(Node nodos[], int numNodos, float costo_padre) {
    list<Connection> c;
    for (auto const& n: connections) {
        for (int i = 0; i < numNodos; ++i) {
            if (nodos[i].getName() == n.first) {
                Node node = nodos[i];
                Connection c_aux(new Node(node.getName(), node.getX(), node.getY(), this, node.getMapConnections()), n.second + costo_padre);
                c.push_front(c_aux);
            }
        }
    }
    return c;
}


Node* Node::getParent() {
    return this->parent;
}

/************************************************
 *                   Overloads                  *
 ************************************************/

bool Node::operator ==(const Node &n) const{
    return (this->name == n.name);
}

bool Node::operator <(const Node &n) const{
    return this->x < n.x;
}

bool Node::operator >(const Node &n) const{
    return this->x > n.x;
}


/************************************************
 *                    Methods                   *
 ************************************************/

void Node::printData() {
    ROS_DEBUG("[Node] Name: %s x:%f y:%f connections: %ld ", name.c_str(), x, y, connections.size());
    if (!parent) {
        ROS_DEBUG(" [No parent]\n");
    } else {
        ROS_DEBUG(" parent: %s\n", parent->getName().c_str());
    }

}

string Node::toString() {
    return this->name;
}

void Node::addConnection(string name, float distance) {
    this->connections.insert(pair<string, float>(name, distance));
}

float Node::getDistance(Node &n) {
    return sqrtf(powf(x - n.getX(), 2) + powf(y - n.y, 2));
}

/************************************************
 *                   Destructor                 *
 ************************************************/
 // TODO: verificar el correcto funcionamiento del destructor
Node::~Node() {
    if(!this->parent) {
        free(parent);
    }
}