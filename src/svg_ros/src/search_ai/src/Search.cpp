/*
 * @author Oropeza Vilchis Luis Alberto (luisunamfi@gmail.com)
 */

#include <ros/console.h>
#include <queue>
#include <stack>
#include <vector>
#include <search_ai/Search.h>


/************************************************
 *                Constructors                  *
 ************************************************/

Search::Search(string filenameNodes, Coord robot, Coord destination){
    this->parser = ParserNodes(filenameNodes);
    this->parser.addRobot(robot.getX(), robot.getY());
    this->parser.addDestination(destination.getX(), destination.getY());
    this->graph = parser.getNodes();
    this->numNodes = parser.getNumberNodes();
    this->start = parser.getNode("robot");
    this->goal = parser.getNode("destination");
}

/************************************************
 *                    Methods                   *
 ************************************************/

list<Node> Search::ucs() {
    vector<Node> expanded;
    priority_queue<Connection> agenda;

    ROS_INFO("Start Dijkstra Algorithm");
    if (start == goal) {
        return path(&start);
    }
    Connection c = Connection(&start, 0.0f);
    agenda.push(c);
    while (!agenda.empty()) {
        Connection n = agenda.top();
        agenda.pop();
        expanded.push_back(Node(n.getNode()[0].getName(), n.getNode()[0].getX(), n.getNode()[0].getY()));
        list<Connection> l = n.getNode()->getConnections(graph, numNodes, n.getCost());
        while(!l.empty()) {
            Connection child = l.front();
            l.pop_front();
            if (child.getNode()[0] == goal) {
                return path(child.getNode());
            }
            Node n_tmp = Node(child.getNode()[0].getName(), child.getNode()[0].getX(), child.getNode()[0].getY());
            if ( std::find(expanded.begin(), expanded.end(), n_tmp) == expanded.end() ) {
                agenda.push(child);
            }
        }
    }
    ROS_ERROR("[Dijkstra] No se encontró una ruta");
    return path(&start);
}

list<Node> Search::gbfs() {
    vector<Node> expanded;
    priority_queue<Connection> agenda;

    ROS_INFO("Start Greedy Best First Search Algorithm");
    if (start == goal) {
        return path(&start);
    }
    Connection c = Connection(&start, 0.0f);
    agenda.push(c);
    while (!agenda.empty()) {
        Connection n = agenda.top();
        agenda.pop();
        expanded.push_back(Node(n.getNode()[0].getName(), n.getNode()[0].getX(), n.getNode()[0].getY()));
        list<Connection> l = n.getNode()->getConnections(graph, numNodes, 0);
        while(!l.empty()) {
            Connection child = l.front();
            l.pop_front();
            if (child.getNode()[0] == goal) {
                return path(child.getNode());
            }
            Node n_tmp = Node(child.getNode()[0].getName(), child.getNode()[0].getX(), child.getNode()[0].getY());
            if ( std::find(expanded.begin(), expanded.end(), n_tmp) == expanded.end() ) {
                child.addHeuristic(child.getNode()->getDistance(goal));
                agenda.push(child);
            }
        }
    }
    ROS_ERROR("[GBFS] No se encontró una ruta");
    return path(&start);
}

list<Node> Search::AStar() {
    std::vector<Node> expanded;
    priority_queue<Connection> agenda;

    ROS_INFO("Start A* Algorithm");
    if (start == goal) {
        return path(&start);
    }
    Connection c = Connection(&start, 0.0f);
    agenda.push(c);
    while (!agenda.empty()) {
        Connection n = agenda.top();
        agenda.pop();
        expanded.push_back(Node(n.getNode()[0].getName(), n.getNode()[0].getX(), n.getNode()[0].getY()));
        list<Connection> l = n.getNode()->getConnections(graph, numNodes, n.getCost());
        while(!l.empty()) {
            Connection child = l.front();
            l.pop_front();
            if (child.getNode()[0] == goal) {
                return path(child.getNode());
            }
            Node n_tmp = Node(child.getNode()[0].getName(), child.getNode()[0].getX(), child.getNode()[0].getY());
            if ( std::find(expanded.begin(), expanded.end(), n_tmp) == expanded.end() ) {
                child.addHeuristic(child.getNode()->getDistance(goal));
                agenda.push(child);
            }
        }
    }
    ROS_ERROR("[A*] No se encontró una ruta");
    return path(&start);
}

list<Node> Search::dfs() {
    vector<Node> expanded;
    stack<Connection> agenda;

    ROS_INFO("Start DFS Algorithm");
    if (start == goal) {
        return path(&start);
    }
    Connection c = Connection(&start, 0.0f);
    agenda.push(c);
    while (!agenda.empty()) {
        Connection n = agenda.top();
        agenda.pop();
        expanded.push_back(Node(n.getNode()[0].getName(), n.getNode()[0].getX(), n.getNode()[0].getY()));
        list<Connection> l = n.getNode()->getConnections(graph, numNodes, 0);
        while(!l.empty()) {
            Connection child = l.front();
            l.pop_front();
            if (child.getNode()[0] == goal) {
                return path(child.getNode());
            }
            Node n_tmp = Node(child.getNode()[0].getName(), child.getNode()[0].getX(), child.getNode()[0].getY());
            if ( std::find(expanded.begin(), expanded.end(), n_tmp) == expanded.end() ) {
                agenda.push(child);
            }
        }
    }
    ROS_ERROR("[DFS] No se encontró una ruta");
    return path(&start);
}

list<Node> Search::bfs() {
    vector<Node> expanded;
    queue<Connection> agenda;

    ROS_INFO("Start BFS Algorithm");
    if (start == goal) {
        return path(&start);
    }
    Connection c = Connection(&start, 0.0f);
    agenda.push(c);
    while (!agenda.empty()) {
        Connection n = agenda.front();
        agenda.pop();
        expanded.push_back(Node(n.getNode()[0].getName(), n.getNode()[0].getX(), n.getNode()[0].getY()));
        list<Connection> l = n.getNode()->getConnections(graph, numNodes, 0);
        while(!l.empty()) {
            Connection child = l.front();
            l.pop_front();
            if (child.getNode()[0] == goal) {
                return path(child.getNode());
            }
            Node n_tmp = Node(child.getNode()[0].getName(), child.getNode()[0].getX(), child.getNode()[0].getY());
            if ( std::find(expanded.begin(), expanded.end(), n_tmp) == expanded.end() ) {
                agenda.push(child);
            }
        }
    }
    ROS_ERROR("[BFS] No se encontró una ruta");
    return path(&start);
}

/**
 * Funcion auxiliar que guarda la ruta encontrada en una lista
 * @param last ultimo nodo de la lista
 * @return lista de nodos ordenados que representan la ruta de inicio a fin
 */
list<Node> Search::path(Node *last) {
    list<Node> sequence;
    do {
        sequence.push_front(last[0]);
        last = last->getParent();
    } while (last != nullptr);
    ROS_INFO("Ruta encontrada %ld:", sequence.size());
    sequence.pop_front(); // Se elimina la posición inicial que es la del robot
    ROS_INFO("Finished search algorithm\n");
    return sequence;
}

/**
 * Funcion auxiliar que imprime la informacion de cada Nodo almacenados en una lista
 * @param l lista de nodos
 */
void Search::printList(list<Node> l) {
    for (auto & e: l) {
        e.printData();
    }
}

/**
 * Funcion auxiliar que imprime la informacion de cada Nodo almacenados en un set
 * @param l set de nodos
 */
void Search::printSet(set<Node> l) {
    for (auto & e: l) {
        Node aux = e;
        aux.printData();
    }
}
