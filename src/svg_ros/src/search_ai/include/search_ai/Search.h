/*
 * @author Oropeza Vilchis Luis Alberto (luisunamfi@gmail.com)
 */

#ifndef _SEARCH_H
#define _SEARCH_H

#include <set>
#include <list>
#include <search_ai/Node.h>
#include <search_ai/ParserNodes.h>
#include <types/Coord.h>

class Search {
public:
    Search(string, Coord, Coord);
    list<Node> ucs();
    list<Node> dfs();
    list<Node> bfs();
    list<Node> gbfs();
    list<Node> AStar();
    list<Node> path(Node *last);
private:
    Node start, goal;
    void printList(list<Node> l);
    void printSet(set<Node> l);
    ParserNodes parser;
    Node *graph;
    int numNodes;
};

#endif
