/*
 * @author Oropeza Vilchis Luis Alberto (luisunamfi@gmail.com)
 */

#ifndef _NODE_H
#define _NODE_H

#include <string>
#include <map>
#include <list>
#include <search_ai/Connection.h>

using namespace std;

class Node {
public:
    Node();
    Node(string name, float x, float y);
    Node(string name, float x, float y, Node *parent);
    Node(string name, float x, float y, Node *parent, map<string, float> connections);
    ~Node();
    string toString();
    void addConnection(string name, float distance);
    void printData();
    float getDistance(Node &n);
    void setParent(Node *p);
    void setName(string name);
    void setPosition(float x, float y);
    void setX(float x);
    void setY(float y);
    Node* getParent();
    string getName();
    map<string, float> getMapConnections();
    float getX();
    float getY();
    list<Node> getNodeConnections(Node nodos[], int numNodes);
    list<Connection> getConnections(Node nodos[], int numNodos, float costo_acumulado);
    bool operator == (const Node &) const;
    bool operator > (const Node &) const;
    bool operator < (const Node &) const;

private:
    string name;
    float x, y;
    Node *parent;
    map<string, float> connections;
};

#endif //SVG_ROS_NODE_H
