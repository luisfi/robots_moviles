/*
 * @author Oropeza Vilchis Luis Alberto (luisunamfi@gmail.com)
 */

#ifndef _PARSER_NODES_H
#define _PARSER_NODES_H

#include <search_ai/Node.h>
#include <string>
#include <list>

using namespace std;

class ParserNodes {

public:
    ParserNodes();
    ParserNodes(string);
    void setNodes(list<Node> nodes);
    void setNumberNodes(int numNodes);
    Node getClosestNode(Node n);
    void setFileName(string fileName);
    Node* getNodes();
    int getNumberNodes();
    string getFileName();
    void printNodes();
    void parse();
    int searchNode(string name);
    void printConnectionsOfNode(int node);
    list<Node> getConnectionsOfNode(int node);
    void addRobot(float x, float y);
    void addDestination(float x, float y);
    void createConnectionRobot();
    void createConnectionDestination();
    void printConnectionsOfNode(string name);
    list<Node> getConnectionsOfNode(string name);
    Node getNode(string name);
    Node getNode(int index);

private:
    Node nodes[2048]; //TODO: repair this
    int numberNodes;
    string fileName;
    void createNode(string name, float x, float);
    void addConnectionNode(string nameNode, string nameDestination, float distance);
};

#endif