/*
 * @author Oropeza Vilchis Luis Alberto (luisunamfi@gmail.com)
 */

#ifndef _CONNECTION_H
#define _CONNECTION_H

class Node;

class Connection {
public:
    Connection(Node *n, float cost);
    void addHeuristic(float h);
    void printData();
    float getCost();
    Node *getNode();
    void setCost(float c);
    void setNode(Node *n);

    bool operator == (const Connection &) const;
    bool operator > (const Connection &) const;
    bool operator < (const Connection &) const;
    bool operator >= (const Connection &) const;
    bool operator <= (const Connection &) const;

private:
    Node *n;
    float cost;
};

#endif
