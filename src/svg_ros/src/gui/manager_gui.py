#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @author: Jesus Savage, UNAM-FI, 11-2014
# adapted for ROS by: Daniel Vargas.
# Modified by: Luis Alberto Oropeza Vilchis (luisunamfi@gmail.com)


from gui_inputs import *
from inputs import *
from parser_data import *
from gui_simulator import *

# ROS Libraries
import rospy
from svg_ros.srv import *


class Planner:
    def __init__(self, window):
        self.gui_data = InputsGUI(window)
        self.gui_data.add_callback_to_element('button_robot',
                                              self.callback_toggle_plot_robot)
        self.gui_data.add_callback_to_element('button_map',
                                              self.callback_toggle_plot_map)
        self.simulator = SimulatorGUI(window, 500, 500)
        self.simulator.add_callback_to_canvas("<Button-1>", self.click_mouse_left)
        self.simulator.add_callback_to_canvas("<Button-2>", self.click_mouse_between)
        self.simulator.add_callback_to_canvas("<Button-3>", self.click_mouse_right)
        self.simulator.add_callback_to_canvas("<Key>", self.key_pressed)
        self.is_plotting = False
        self.flag_over = False
        self.num_steps_ros = 1
        self.delay = 0.0
        self.num_sensors = 2
        self.range_angle = 0.2
        self.start_angle = -0.1
        self.path = 'path'
        self.robot_command = "../motion_planner/GoTo_State_Machine"
        self.click_left = [0.0, 0.0]
        self.click_right = [0.0, 0.0]
        self.click_between = [0.0, 0.0]
        self.polygons = []
        self.num_pol = 0
        self.pose_x = 4.0
        self.pose_y = 3.0
        self.pose_theta = 0.0
        self.radio_robot = .03
        self.dim_x = 10.0
        self.dim_y = 10.0
        self.number_steps = 100
        self.event_mouse_3 = 0
        self.destination_x = 0
        self.destination_y = 0
        self.plot_map(1)

        rospy.init_node('mv_gui')
        s = rospy.Service('environment', EnvironmentSrv, self.send_values)
        s = rospy.Service('mv_gui_comm', MVServ, self.handle_mv_plot)
        s = rospy.Service('over_gui', OverSrv, self.handle_over)

    """
    Callback that receives ROS messages
    It prints motion_planner get_info
    for real time plotting
    """
    def handle_mv_plot(self, req):
        if self.flag_over == 0:
            self.is_plotting = True
            self.plot_line_read(req.param, 0)
        return MVServResponse("_")

    """
        Function that it is triggered when motion_planner has ended
    """
    def handle_over(self, req):
        self.flag_over = True
        self.is_plotting = True
        self.toggle_plot_execute()
        self.is_plotting = False
        self.flag_over = False
        self.num_steps_ros = 1
        return OverSrvResponse(1)

    """
    Callback que manda los valores de las banderas del turtle bot
    """
    def send_values(self, req):
        resp = EnvironmentSrvResponse()
        resp.sensor = self.gui_data.var_laser_sensor.get()
        resp.light = self.gui_data.var_light_sensor.get()
        resp.base = self.gui_data.var_turtlebot.get()
        resp.invert = self.gui_data.var_inverted_sensor.get()
        resp.planner = self.gui_data.var_planner.get()
        return resp

    def send_inputs_client(self):
        i = Inputs()
        i.xPositionOrigin = self.click_left[0]
        i.yPositionOrigin = self.click_left[1]
        i.anglePosition = float(self.gui_data.get_robot_angle())
        self.destination_x = self.click_right[0]
        self.destination_y = self.click_right[1]
        i.xPositionTarget = self.click_right[0]
        i.yPositionTarget = self.click_right[1]
        i.sensorName = str(self.gui_data.var_sensor)
        i.numberSensors = int(self.gui_data.get_num_sensors())
        i.angleSensor = float(self.gui_data.get_orig_angle())
        i.rangeSensor = float(self.gui_data.get_range_angle())
        i.radiusRobot = float(self.gui_data.get_robot_radio())
        i.magnitudeAdvance = float(self.gui_data.get_robot_advance())
        i.maxAngle = float(self.gui_data.get_max_angle())
        i.numberSteps = int(self.gui_data.get_num_steps())
        i.optionSelected = int(self.gui_data.get_behaviour())
        i.largestSensorValue = float(self.gui_data.get_largest_value())
        i.pathToFiles = self.gui_data.get_path()
        i.fileEnvironment = self.gui_data.get_filename()
        i.activateNoise = self.gui_data.var_noise.get()
        i.d0 = float(self.gui_data.get_d0())
        i.eta = float(self.gui_data.get_eta())
        i.d1 = float(self.gui_data.get_d1())
        i.epsilon = float(self.gui_data.get_epsilon())
        i.delta = float(self.gui_data.get_delta())
        i.send_inputs()

    def plot_robot(self):
        pose = (self.pose_x, self.pose_y, self.pose_theta,)
        self.simulator.plot_robot(pose, self.radio_robot, self.dim_x, self.dim_y)
        self.click_right[0] = self.pose_x
        self.click_right[1] = self.pose_y

    def plot_map(self, flg):
        self.simulator.canvas.create_rectangle(0, 0, self.simulator.size_x, self.simulator.size_y, fill="black")
        self.path = self.gui_data.get_path()
        path = self.path + self.gui_data.get_filename() + '.wrl'
        data = ParserData.parse_file_map(path)
        self.dim_x = data['dimensions'][0]
        self.dim_y = data['dimensions'][1]
        for i in data['polygons']:
            p = self.simulator.plot_polygon(i, self.dim_x, self.dim_y)
            if flg == 1:
                self.polygons.append(p)
                self.num_pol += 1

    def plot_line_read(self, line, flg, color="#FFD1AA"):
        flg_mov = self.gui_data.var_mov.get()
        flg_sensor = self.gui_data.var_sensor.get()
        key, data = ParserData.parse_line(line, self.dim_x, self.dim_y)
        if key == 'polygon':
            p = self.simulator.plot_polygon(data[key], self.dim_x, self.dim_y)
            if flg == 1:
                self.polygons.append(p)
                self.num_pol += 1
        elif key == "dimensions":
            self.dim_x = data[key][0]
            self.dim_y = data[key][1]
        elif key == "radio_robot":
            self.radio_robot = data[key]
        elif key == "destination":
            self.simulator.plot_oval(data[key][0], data[key][1], self.dim_x, self.dim_y, self.radio_robot)
        elif key == "robot":
            self.pose_x = data[key][0]
            self.pose_y = data[key][1]
            self.pose_theta = data[key][2]
            self.gui_data.set_robot_angle(str(self.pose_theta))
            self.gui_data.set_pose_x(self.pose_x)
            self.gui_data.set_pose_y(self.pose_y)
            self.gui_data.set_num_steps(str(self.num_steps_ros))
            self.num_steps_ros += 1
            if flg_mov == 1:
                self.simulator.canvas.delete('all')
                self.plot_map(0)
            self.plot_robot()
            x = self.destination_x
            y = self.destination_y
            self.simulator.plot_oval(x, y, self.dim_x, self.dim_y, self.radio_robot)
            if self.gui_data.var_mov.get() == 0:
                self.simulator.canvas.update_idletasks()
            else:
                if flg_sensor == 0:
                    self.simulator.canvas.update_idletasks()
            self.simulator.canvas.update_idletasks()
        elif key == "sensor":
            if flg_sensor == 1:
                self.pose_x = data[key][0]
                self.pose_y = data[key][1]
                self.pose_theta = data[key][2]
                self.plot_robot()
                self.range_angle = data[key][4]
                self.start_angle = data[key][5]
                line = data[key][-1][0]
                self.simulator.plot_line(line[0], line[1], line[2], line[3], "red", 1, self.dim_x, self.dim_y)
                for i in data[key][-1][1:]:
                    self.simulator.plot_line(i[0], i[1], i[2], i[3], color, 0, self.dim_x, self.dim_y)
                    self.simulator.canvas.update_idletasks()
                self.simulator.plot_oval(self.destination_x, self.destination_y, self.dim_x, self.dim_y, self.radio_robot)
                self.simulator.canvas.update_idletasks()
        self.simulator.canvas.update_idletasks()

    def plot_previous_execute(self):
        self.path = self.gui_data.get_path()
        path = self.path + self.gui_data.get_filename() + '.raw'
        self.simulator.canvas.update_idletasks()  # it updates the ide data
        with open(path, 'r') as file_data:
            self.delay = float(self.gui_data.get_delay())
            for line in file_data:
                self.plot_line_read(line, 0, 'cyan')
        self.gui_data.set_num_steps(self.number_steps)

    def callback_toggle_plot_robot(self):
        self.simulator.canvas.update_idletasks()
        self.path = self.gui_data.get_path()
        self.plot_map(0)
        self.plot_previous_execute()

    def callback_toggle_plot_map(self):
        self.simulator.canvas.update_idletasks()
        self.plot_map(0)
        self.gui_data.set_robot_angle('0.0')
        self.gui_data.set_num_steps(self.number_steps)

    def start_plot_execute(self):
        self.plot_map(0)
        self.send_inputs_client()

    def toggle_plot_execute(self):
        self.num_steps_ros = 1
        self.delay = float(self.gui_data.get_delay())
        self.plot_map(0)
        self.plot_previous_execute()
        self.click_left[0] = self.pose_x
        self.click_left[1] = self.pose_y

    def click_mouse_left(self, event):
        if not self.is_plotting:
            self.simulator.canvas.delete('all')
            self.plot_map(0)
            x = (self.dim_x * event.x) / self.simulator.size_x
            y = (self.dim_y * (self.simulator.size_y - event.y)) / self.simulator.size_y
            self.click_left[0] = x
            self.click_left[1] = y
            self.pose_theta = float(self.gui_data.get_robot_angle())
            self.pose_x = x
            self.pose_y = y
            self.plot_robot()

    def click_mouse_between(self, event):
        self.simulator.canvas.create_rectangle(event.x, event.y, event.x + 5, event.y + 5, fill="blue", outline='yellow')
        x = (self.dim_x * event.x) / self.simulator.size_x
        y = (self.dim_y * (self.simulator.size_y - event.y)) / self.simulator.size_y
        self.click_between[0] = x
        self.click_between[1] = y
        print('({0}, {1}) ({2}, {3})'.format(event.x, event.y, x, y))

    # Dibuja un pequeño cuadro en el canvas en las coordenadas (x, y)
    def draw_point(self, x, y):
        x1 = (x * self.simulator.size_x) / self.dim_x
        y1 = self.simulator.size_y - (y * self.simulator.size_y) / self.dim_y
        self.simulator.canvas.create_rectangle(x1, y1, x1 + 3, y1 + 3, fill="red", outline='yellow')


    def key_pressed(self, event):
        # TODO: make stop execution with some stroke
        print("Pressed ", repr(event.char))
        # Si se presiona 'd' se dibujan los nodos del planner
        if event.char == 'd':
            self.flag_over = 1
            self.draw_point(0.68, 1.1)
            self.draw_point(0.29, 0.93)
            self.draw_point(0.39, 0.58)
            self.draw_point(0.13, 0.24)
            self.draw_point(0.4, 0.28)
            self.draw_point(0.67, 0.29)
            self.draw_point(0.72, 0.89)
            self.draw_point(0.6, 0.57)
            self.draw_point(0.38, 0.16)
            self.draw_point(0.19, 1.13)
            self.draw_point(0.65, 0.82)
            self.draw_point(0.63, 0.17)
            self.draw_point(0.59, 0.25)
            self.draw_point(0.73, 0.21)



    def click_mouse_right(self, event):
        if not self.is_plotting:
            x = (self.dim_x * event.x) / self.simulator.size_x
            y = (self.dim_y * (self.simulator.size_y - event.y)) / self.simulator.size_y
            self.click_right[0] = x
            self.click_right[1] = y
            if self.gui_data.var_mov.get() == 0:
                self.simulator.canvas.update_idletasks()
            self.simulator.canvas.update_idletasks()
            self.event_mouse_3 = 1
            self.start_plot_execute()
            self.simulator.plot_oval(x, y, self.dim_x, self.dim_y, self.radio_robot);
            self.number_steps = self.gui_data.get_num_steps()


if __name__ == '__main__':
    root = Tk.Tk()
    root.withdraw()  # Hide Window
    root.wm_title('Planner ROS')
    root.geometry("%dx%d" % (1030, 510))
    root.resizable(False, False)
    gui_planner = Planner(root)
    root.wm_deiconify()  # Show window
    gui_planner.simulator.canvas.focus_set()
    print("GUI node started")
    Tk.mainloop()
