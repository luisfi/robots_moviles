#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @author: Luis Alberto Oropeza Vilchis (luisunamfi@gmail.com)
# UNAM-FI, 2018

import rospy
from svg_ros.srv import *


class Inputs:
    """
    Clase que maneja los datos de las entradas de la GUI y el servicio para enviarlas
    """

    def __init__(self):
        self.xPositionOrigin = 0.0
        self.yPositionOrigin = 0.0
        self.xPositionTarget = 0.0
        self.yPositionTarget = 0.0
        self.anglePosition = 0.0
        self.angleSensor = 0.0
        self.rangeSensor = 0.0
        self.radiusRobot = 0.0
        self.magnitudeAdvance = 0.0
        self.maxAngle = 0.0
        self.largestSensorValue = 0.0
        self.sensorName = ''
        self.pathToFiles = ''
        self.fileEnvironment = ''
        self.numberSensors= 0
        self.numberSteps= 0
        self.optionSelected = 0
        self.activateNoise= 0
        self.d0 = 0.0
        self.eta = 0.0
        self.d1 = 0.0
        self.epsilon = 0.0
        self.delta = 0.0

    def send_inputs(self):
        rospy.wait_for_service('sendInputs')
        try:
            send_inputs = rospy.ServiceProxy('sendInputs', InputsSrv)
            resp = send_inputs(self.xPositionOrigin, self.yPositionOrigin, self.xPositionTarget, self.yPositionTarget,
                               self.anglePosition, self.angleSensor, self.rangeSensor, self.radiusRobot,
                               self.magnitudeAdvance, self.maxAngle, self.largestSensorValue, self.sensorName,
                               self.pathToFiles, self.fileEnvironment, self.numberSensors, self.numberSteps,
                               self.optionSelected,   self.activateNoise, self.d0, self.eta, self.d1, self.epsilon,
                               self.delta)
        except rospy.ServiceException, e:
            print("Service call failed {0}".format(e))
        return resp.success