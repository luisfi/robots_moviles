#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @author:  Luis Alberto Oropeza Vilchis (luisunamfi@gmail.com) UNAM-FI, 2018

import math


class ParserData:
    """
    Clase de soporte para obtener los datos para simular el movimiento del robot
    """
    def __init__(self):
        pass

    @staticmethod
    def parse_file_map(file):
        data = dict()
        polygons = list()
        with open(file, 'r') as world_file:
            for line in world_file:
                polygon = list()
                words = line.split()
                if len(words) > 1:
                    if words[0] == ';(':
                        continue
                    if words[1] == 'dimensions':
                        data['dimensions'] = [float(words[3]), float(words[4])]
                    elif words[1] == 'polygon':
                        for i in words[4:-1]:
                            polygon.append(float(i))
                        polygons.append(polygon)
            data['polygons'] = polygons
            return data

    @staticmethod
    def parse_line(line, dim_x, dim_y):
        words = line.split()
        data = dict()
        polygon = list()
        key = ''
        if len(words) > 1:
            if words[1] == 'polygon':
                key = 'polygon'
                for i in words[4:-1]:
                    polygon.append(float(i))
                data['polygon'] = polygon
            elif words[1] == "dimensions":
                key = 'dimensions'
                data['dimensions'] = [float(words[3]), float(words[4])]
            elif words[1] == "radio_robot":
                key = 'radio_robot'
                data['radio_robot'] = float(words[2])
                data['radio_robot'] = float(words[2])
            elif words[1] == "destination":
                key = 'destination'
                data['destination'] = [float(words[2]), float(words[3])]
            elif words[1] == "robot":
                key = 'robot'
                data['robot'] = [float(words[3]), float(words[4]), float(words[5])]
            elif words[1] == "sensor":
                key = 'sensor'
                data['sensor'] = [float(words[3]), float(words[4]), float(words[5]), int(words[6]),
                                  float(words[7]), float(words[8])]
                lines = list()
                x1 = float(words[3])
                y1 = float(words[4])
                if int(words[6]) == 1:
                    delta_angle = float(words[7])
                else:
                    delta_angle = float(words[7]) / (float(words[6]) - 1)
                angle_robot = float(words[5])
                theta = angle_robot + float(words[8])
                x2 = x1 + (dim_x / 10) * math.cos(angle_robot)
                y2 = y1 + (dim_y / 10) * math.sin(angle_robot)
                lines.append((x1, y1, x2, y2))
                for i in range(9, len(words) - 1):
                    v = float(words[i])
                    x2 = x1 + v * math.cos(theta)
                    y2 = y1 + v * math.sin(theta)
                    lines.append((x1, y1, x2, y2,))
                    theta = theta + delta_angle
                data['sensor'].append(lines)
        return key, data
