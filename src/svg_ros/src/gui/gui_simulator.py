#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @author: Luis Alberto Oropeza Vilchis (luisunamfi@gmail.com)
# UNAM-FI, 2018


import Tkinter as Tk
import math


class SimulatorGUI:
    """
    Clase para llevar el control del canvas donde se dibuja la simulación
    """

    def __init__(self, root, size_x=400, size_y=400):
        self.size_x = size_x
        self.size_y = size_y
        self.frame_canvas = Tk.Frame(root, width=size_x, height=size_y)
        self.canvas = Tk.Canvas(self.frame_canvas, bg='white', width=size_x, height=size_y)
        self.canvas.pack(expand=False)
        self.frame_canvas.grid({'row': 0, 'column': 1})
        self.frame_canvas.focus_set()

    def plot_polygon(self,  data, dim_x, dim_y):
        coordinates = list()
        for i in zip(data[0::2], data[1::2]):
            x = (self.size_x * i[0]) / dim_x
            y = self.size_y - (self.size_x * i[1]) / dim_y
            coordinates.append(x)
            coordinates.append(y)
        return self.canvas.create_polygon(coordinates, outline='white', fill='red', width=1)

    def plot_line(self, x1, y1, x2, y2, color, flg, dim_x, dim_y):
        x_normalized = (self.size_x * x1) / dim_x
        y_normalized = self.size_y - (self.size_y * y1) / dim_y
        x2_normalized = (self.size_x * x2) / dim_x
        y2_normalized = self.size_y - (self.size_y * y2) / dim_y
        if flg == 1:
            self.canvas.create_line(x_normalized, y_normalized, x2_normalized, y2_normalized, fill=color, arrow="last")
        else:
            self.canvas.create_line(x_normalized, y_normalized, x2_normalized, y2_normalized, fill=color)
            self.canvas.create_rectangle(x2_normalized, y2_normalized, x2_normalized + 1, y2_normalized + 1, fill="white", outline="white")

    def plot_oval(self, x_click, y_click, dim_x, dim_y, radio_robot):
        x = (self.size_x * x_click) / dim_x
        y = self.size_y - (self.size_y * y_click) / dim_y
        robot_radio = (self.size_x * radio_robot) / dim_x
        x1 = x - robot_radio
        y1 = y - robot_radio
        x2 = x + robot_radio
        y2 = y + robot_radio
        return self.canvas.create_oval(x1, y1, x2, y2, outline="green", fill="yellow", width=1)

    def plot_robot(self, pose, radio_robot, dim_x, dim_y):
        pose_x = pose[0]
        pose_y = pose[1]
        pose_theta = pose[2]
        x = (self.size_x * pose_x) / dim_x
        y = self.size_y - (self.size_y * pose_y) / dim_y
        robot_radio = (self.size_x * radio_robot) / dim_x
        x1 = x - robot_radio / 2
        y1 = y - robot_radio / 2
        x2 = x + robot_radio / 2
        y2 = y + robot_radio / 2
        self.canvas.create_oval(x1, y1, x2, y2, outline="yellow", fill="green", width=1)
        angle_robot = pose_theta
        x2 = pose_x + (dim_x / 25) * math.cos(angle_robot)
        y2 = pose_y + (dim_y / 25) * math.sin(angle_robot)
        self.plot_line(pose_x, pose_y, x2, y2, "white", 1, dim_x, dim_y)

    def add_callback_to_canvas(self, name, callback):
        self.canvas.bind(name, callback)
