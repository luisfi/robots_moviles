#!/usr/bin/env python
# -*- coding: utf-8 -*-

# @author: Luis Alberto Oropeza Vilchis (luisunamfi@gmail.com)
# UNAM-FI, 2018

from Tkinter import *
from Tkinter import IntVar
import rospy

class InputsGUI:
    """
    Clase que maneja la GUI de las entradas para el simulador
    """

    def __init__(self, root):
        self.frame = Frame(root);
        self.elements = dict()  # Saves GUI elements
        self.callbacks = dict()  # Saves button callbacks
        self.options = list()
        self.countRobot = 0
        self.countExecute = 0
        self.countMap = 0
        self.countPath = 0
        # Variables para los CheckButton
        self.var_mov = IntVar()
        self.var_sensor = IntVar()
        self.var_noise = IntVar()
        self.var_laser_sensor = IntVar()
        self.var_light_sensor = IntVar()
        self.var_turtlebot = IntVar()
        self.var_planner = IntVar()
        self.var_inverted_sensor = IntVar()
        self.var_option_behaviour = StringVar(root)
        self.setup_gui()
        self.frame.grid({'row': 0, 'column': 0})

    def setup_gui(self):
        # Button plot previous
        self.elements['button_robot'] = Button(self.frame, width=20, text='Plot Previous Robot\'s Path',
                                               bg='green', activebackground='green', command=self.toggle_plot_robot)
        # Plot Map button
        self.elements['button_map'] = Button(self.frame, width=20, text='Plot Map', bg='green',
                                             activebackground='green', command=self.toggle_plot_map)

        # Path files entry
        self.elements['label_path'] = Label(self.frame, text='Data Path')
        self.elements['entry_path'] = Entry(self.frame, width=15, foreground='black', background='green')
        self.elements['entry_path'].insert(0, '../data/')

        # World's File entry
        self.elements['label_file'] = Label(self.frame, text='Environment File')
        self.elements['entry_file'] = Entry(self.frame, width=15, foreground='black', background='green')
        self.elements['entry_file'].insert(0, 'final')

        # Delay plotting
        self.elements['label_delay'] = Label(self.frame, text='Delay plotting')
        self.elements['entry_delay'] = Entry(self.frame, width=8, foreground='black', background='green')
        self.elements['entry_delay'].insert(0, '0.0')

        # Show sensors
        self.elements['checkb_sensor'] = Checkbutton(self.frame, text="show sensors", variable=self.var_sensor)
        self.elements['checkb_sensor'].select()
        self.var_sensor.set(1)

        # Movement
        self.elements['checkb_movement'] = Checkbutton(self.frame, text="Show robot's single movements",
                                                       variable=self.var_mov)
        self.elements['checkb_movement'].deselect()
        self.var_mov.set(0)

        # Add Noise
        self.elements['checkb_noise'] = Checkbutton(self.frame, text="Add noise", variable=self.var_noise)
        self.elements['checkb_noise'].select()
        self.var_noise.set(1)

        # Laser sensor turtle bot
        self.elements['checkb_laser_sensor'] = Checkbutton(self.frame, text="Laser Sensor",
                                                           variable=self.var_laser_sensor)
        self.elements['checkb_laser_sensor'].deselect()
        self.var_laser_sensor.set(0)

        # Light Sensor
        self.elements['checkb_light_sensor'] = Checkbutton(self.frame, text="Light Sensor", variable=self.var_light_sensor)
        self.elements['checkb_light_sensor'].deselect()
        self.var_mov.set(0)

        # Turtlebot
        self.elements['checkb_turtlebot'] = Checkbutton(self.frame, text="Turtlebot", variable=self.var_turtlebot)
        self.elements['checkb_turtlebot'].deselect()
        self.var_turtlebot.set(0)

        # Planner
        self.elements['checkb_planner'] = Checkbutton(self.frame, text="Planner", variable=self.var_planner)
        self.elements['checkb_planner'].deselect()
        self.var_planner.set(1)

        # Inverted noise
        self.elements['checkb_inverted_sensor'] = Checkbutton(self.frame, text="Inverted sensor", variable=self.var_inverted_sensor)
        self.elements['checkb_inverted_sensor'].deselect()
        self.var_inverted_sensor.set(0)

        # Number of sensors
        self.elements['label_num_sensors'] = Label(self.frame, text='Num. Sensors')
        self.elements['entry_num_sensors'] = Entry(self.frame, width=8, foreground='black', background='green')
        self.elements['entry_num_sensors'].insert(0, '2')

        # Origin angle
        self.elements['label_orig_angle'] = Label(self.frame, text='Origen angle sensor ')
        self.elements['entry_orig_angle'] = Entry(self.frame, width=8, foreground='black', background='green')
        self.elements['entry_orig_angle'].insert(0, '-0.2')

        # Range angle sensor
        self.elements['label_range_angle'] = Label(self.frame, text='Range angle sensor ')
        self.elements['entry_range_angle'] = Entry(self.frame, width=8, foreground='black', background='green')
        self.elements['entry_range_angle'].insert(0, '0.4')

        # Robot's magnitude advance
        self.elements['label_advance_robot'] = Label(self.frame, text="Robot's magnitude advance")
        self.elements['entry_robot_advance'] = Entry(self.frame, width=8, foreground='black',
                                                     background='green')
        self.elements['entry_robot_advance'].insert(0, '0.04')

        # Robot's maximum angle
        self.elements['label_max_angle'] = Label(self.frame, text="Robot's maximum turn angle")
        self.elements['entry_max_angle'] = Entry(self.frame, width=8, foreground='black', background='green')
        self.elements['entry_max_angle'].insert(0, '0.7854')

        # Robot's radio
        self.elements['label_radio_robot'] = Label(self.frame, text="Robot's radio")
        self.elements['entry_robot_radio'] = Entry(self.frame, width=8, foreground='black', background='green')
        self.elements['entry_robot_radio'].insert(0, '0.03')

        # Robot's pose x
        self.elements['label_pose_x'] = Label(self.frame, text="Robot's pose x")
        self.elements['entry_pose_x'] = Entry(self.frame, width=8, foreground='black', background='green')
        self.elements['entry_pose_x'].insert(0, '4.0')

        # Robot's pose y
        self.elements['label_pose_y'] = Label(self.frame, text="Robot's pose y")
        self.elements['entry_pose_y'] = Entry(self.frame, width=8, foreground='black', background='green')
        self.elements['entry_pose_y'].insert(0, '5.0')

        # Robot's angle
        self.elements['label_robot_angle'] = Label(self.frame, text="Robot's angle")
        self.elements['entry_robot_angle'] = Entry(self.frame, width=8, foreground='black', background='green')
        self.elements['entry_robot_angle'].insert(0, '0.0')

        # Robot's command
        # self.label_robot_command = Label(self.topLevelWindow,text =  "Robot's command")
        self.elements['label_left_button'] = Label(self.frame, text="Left mouse's button selects origen,")
        self.elements['label_right_button'] = Label(self.frame, text="right button selects destination")
        self.elements['entry_robot_command'] = Entry(self.frame, width=30, foreground='black',
                                                     background='green')
        self.elements['entry_robot_command'].insert(0, "../motion_planner/GoTo_State_Machine")

        # Number of steps
        self.elements['label_num_steps'] = Label(self.frame, text="Number of Steps")
        self.elements['entry_num_steps'] = Entry(self.frame, width=8, foreground='black', background='green')
        self.elements['entry_num_steps'].insert(0, '150')

        # Selection of behavior
        # self.elements['label_behaviour'] = Label(self.frame, text="Behaviour Selection")
        # self.elements['entry_behaviour'] = Entry(self.frame, width=8, foreground='black', background='green')
        # self.elements['entry_behaviour'].insert(0, '6')
        self.options = ['Avoidance', 'Destination', 'Avoid-Dest', 'Surround', 'Follow Wall',
                        'Potential Fields', 'DFS', 'BFS', 'Dijkstra', 'GBFS', 'A*', 'Dijkstra-Fields']
        self.elements['label_behaviour'] = Label(self.frame, text="Behaviour Selection")
        self.elements['option_menu_behaviour'] = OptionMenu(self.frame, self.var_option_behaviour, *self.options)
        self.elements['option_menu_behaviour'].config(bg='green', width=8, fg='black')
        self.var_option_behaviour.set(self.options[10])

        # Largest value sensor
        self.elements['label_largest'] = Label(self.frame, text="Largest value sensor")
        self.elements['entry_largest'] = Entry(self.frame, width=8, foreground='black', background='green')
        self.elements['entry_largest'].insert(0, '0.3')

        # Potential Fields
        self.elements['label_d0'] = Label(self.frame, text="d0 (repulsive)")
        self.elements['entry_d0'] = Entry(self.frame, width=8, foreground='black', background='green')
        self.elements['entry_d0'].insert(0, '0.15')

        self.elements['label_eta'] = Label(self.frame, text="eta (repulsive)")
        self.elements['entry_eta'] = Entry(self.frame, width=8, foreground='black', background='green')
        self.elements['entry_eta'].insert(0, '0.002')

        self.elements['label_d1'] = Label(self.frame, text="d1 (attractive)")
        self.elements['entry_d1'] = Entry(self.frame, width=8, foreground='black', background='green')
        self.elements['entry_d1'].insert(0, '0.5')

        self.elements['label_epsilon'] = Label(self.frame, text="epsilon (attractive)")
        self.elements['entry_epsilon'] = Entry(self.frame, width=8, foreground='black', background='green')
        self.elements['entry_epsilon'].insert(0, '30.0')

        self.elements['label_delta'] = Label(self.frame, text="delta (total force)")
        self.elements['entry_delta'] = Entry(self.frame, width=8, foreground='black', background='green')
        self.elements['entry_delta'].insert(0, '1.0')

        # GRIDS
        self.elements['label_path'].grid({'row': 1, 'column': 0})
        self.elements['label_file'].grid({'row': 2, 'column': 0})
        self.elements['label_pose_x'].grid({'row': 4, 'column': 0})
        self.elements['label_pose_y'].grid({'row': 5, 'column': 0})
        self.elements['label_robot_angle'].grid({'row': 6, 'column': 0})
        self.elements['label_behaviour'].grid({'row': 7, 'column': 0})
        self.elements['label_largest'].grid({'row': 8, 'column': 0})
        self.elements['entry_path'].grid({'row': 1, 'column': 1})
        self.elements['entry_file'].grid({'row': 2, 'column': 1})
        self.elements['entry_pose_x'].grid({'row': 4, 'column': 1})
        self.elements['entry_pose_y'].grid({'row': 5, 'column': 1})
        self.elements['entry_robot_angle'].grid({'row': 6, 'column': 1})
        self.elements['option_menu_behaviour'].grid({'row': 7, 'column': 1}, sticky="ew ")
        self.elements['entry_largest'].grid({'row': 8, 'column': 1})
        self.elements['checkb_movement'].grid({'row': 0, 'column': 2})
        self.elements['checkb_sensor'].grid({'row': 1, 'column': 2})
        self.elements['checkb_noise'].grid({'row': 2, 'column': 2})
        self.elements['button_map'].grid({'row': 3, 'column': 2})
        self.elements['button_robot'].grid({'row': 4, 'column': 2})
        self.elements['label_left_button'].grid({'row': 5, 'column': 2})
        self.elements['label_right_button'].grid({'row': 6, 'column': 2})
        self.elements['checkb_laser_sensor'].grid({'row': 7, 'column': 2})
        self.elements['checkb_light_sensor'].grid({'row': 8, 'column': 2})
        self.elements['checkb_turtlebot'].grid({'row': 9, 'column': 2})
        self.elements['checkb_inverted_sensor'].grid({'row': 10, 'column': 2})
        self.elements['checkb_planner'].grid({'row': 11, 'column': 2})
        self.elements['label_orig_angle'].grid({'row': 9, 'column': 0})
        self.elements['label_num_sensors'].grid({'row': 10, 'column': 0})
        self.elements['label_range_angle'].grid({'row': 11, 'column': 0})
        self.elements['label_radio_robot'].grid({'row': 12, 'column': 0})
        self.elements['label_advance_robot'].grid({'row': 13, 'column': 0})
        self.elements['label_max_angle'].grid({'row': 14, 'column': 0})
        self.elements['label_num_steps'].grid({'row': 15, 'column': 0})
        self.elements['label_d0'].grid({'row': 16, 'column': 0})
        self.elements['label_eta'].grid({'row': 17, 'column': 0})
        self.elements['label_d1'].grid({'row': 18, 'column': 0})
        self.elements['label_epsilon'].grid({'row': 19, 'column': 0})
        self.elements['label_delta'].grid({'row': 20, 'column': 0})
        self.elements['entry_orig_angle'].grid({'row': 9, 'column': 1})
        self.elements['entry_num_sensors'].grid({'row': 10, 'column': 1})
        self.elements['entry_range_angle'].grid({'row': 11, 'column': 1})
        self.elements['entry_robot_radio'].grid({'row': 12, 'column': 1})
        self.elements['entry_robot_advance'].grid({'row': 13, 'column': 1})
        self.elements['entry_max_angle'].grid({'row': 14, 'column': 1})
        self.elements['entry_num_steps'].grid({'row': 15, 'column': 1})
        self.elements['entry_d0'].grid({'row': 16, 'column': 1})
        self.elements['entry_eta'].grid({'row': 17, 'column': 1})
        self.elements['entry_d1'].grid({'row': 18, 'column': 1})
        self.elements['entry_epsilon'].grid({'row': 19, 'column': 1})
        self.elements['entry_delta'].grid({'row': 20, 'column': 1})

    def toggle_plot_robot(self):
        self.elements['button_robot']['bg'] = 'red'
        self.elements['button_robot']['activebackground'] = 'red'
        self.execute_callbacks('button_robot')
        self.elements['button_robot']['bg'] = 'green'
        self.elements['button_robot']['activebackground'] = 'green'

    def toggle_plot_map(self):
        self.elements['button_map']['bg'] = 'red'
        self.elements['button_map']['activebackground'] = 'red'
        self.execute_callbacks('button_map')
        self.elements['button_map']['bg'] = 'green'
        self.elements['button_map']['activebackground'] = 'green'
        self.set_robot_angle("0.0")

    """
    Agrega un callback para que sea ejecutado por el elemnto correspodiente
    """
    def add_callback_to_element(self, name, callback):
        if not self.callbacks.get(name):
            self.callbacks[name] = list()
        self.callbacks[name].append(callback)

    """
    Función que ejecuta los callbacks correspondientes al elemento name
    """
    def execute_callbacks(self, name):
        try:
            # rospy.logdebug('[toggle_plot_map.py] Executing callbacks by {0}'.format(name))
            for callback in self.callbacks[name]:
                callback()
        except KeyError:
            rospy.logerr('[toggle_plot_map.py] [Error] No hay callbacks para {0}'.format(name))

    # Getters y Setters de los entries de la GUI
    # TODO: verificar el nombre correcto de la excepción que se puede lanzar

    def get_entry_value(self, name):
        try:
            # rospy.logdebug('[get_entry_value] Value from {0} is {1}'.format(name, self.elements[name].get()) )
            return self.elements[name].get()
        except Exception:
            rospy.logerr('[gui_inputs.py][get_entry_value] Error no se pudo obtener el valor de {0}'.format(name))

    def set_entry_value(self, name, value):
        try:
            self.elements[name].delete(0, END)
            self.elements[name].insert(0, value)
        except Exception:
            rospy.logerr('[gui_inputs.py][set_entry_value] Error no se pudo insertar el valor de {0}'.format(name))

    def get_robot_angle(self):
        return self.get_entry_value('entry_robot_angle')
    
    def get_path(self):
        return self.get_entry_value('entry_path')

    def get_filename(self):
        return self.get_entry_value('entry_file')

    def get_delay(self):
        return self.get_entry_value('entry_delay')

    def get_pose_x(self):
        return self.get_entry_value('entry_pose_x')

    def get_pose_y(self):
        return self.get_entry_value('entry_pose_y')

    def get_largest_value(self):
        return self.get_entry_value('entry_largest')

    def get_max_angle(self):
        return self.get_entry_value('entry_max_angle')

    def get_behaviour(self):
        value = self.var_option_behaviour.get()
        if value == self.options[0]:
            return 1
        if value == self.options[1]:
            return 2
        if value == self.options[2]:
            return 3
        if value == self.options[3]:
            return 4
        if value == self.options[4]:
            return 5
        if value == self.options[5]:
            return 6
        if value == self.options[6]:
            return 7
        if value == self.options[7]:
            return 8
        if value == self.options[8]:
            return 9
        if value == self.options[9]:
            return 10
        if value == self.options[10]:
            return 11
        if value == self.options[11]:
            return 12

    def get_num_steps(self):
        return self.get_entry_value('entry_num_steps')

    def get_orig_angle(self):
        return self.get_entry_value('entry_orig_angle')

    def get_num_sensors(self):
        return self.get_entry_value('entry_num_sensors')

    def get_range_angle(self):
        return self.get_entry_value('entry_range_angle')

    def get_robot_radio(self):
        return self.get_entry_value('entry_robot_radio')
    
    def get_robot_command(self):
        return self.get_entry_value('entry_robot_command')
    
    def get_robot_advance(self):
        return self.get_entry_value('entry_robot_advance')

    def get_d0(self):
        return self.get_entry_value('entry_d0')

    def get_eta(self):
        return self.get_entry_value('entry_eta')

    def get_d1(self):
        return self.get_entry_value('entry_d1')

    def get_epsilon(self):
        return self.get_entry_value('entry_epsilon')

    def get_delta(self):
        return self.get_entry_value('entry_delta')

    def set_robot_angle(self, value):
        return self.set_entry_value('entry_robot_angle', value)

    def set_path(self, value):
        return self.set_entry_value('entry_path', value)

    def set_filename(self, value):
        return self.set_entry_value('entry_file', value)

    def set_delay(self, value):
        return self.set_entry_value('entry_delay', value)

    def set_pose_x(self, value):
        return self.set_entry_value('entry_pose_x', value)

    def set_pose_y(self, value):
        return self.set_entry_value('entry_pose_y', value)

    def set_largest_value(self, value):
        return self.set_entry_value('entry_largest', value)

    def set_max_angle(self, value):
        return self.set_entry_value('entry_max_angle', value)

    def set_behaviour(self, value):
        return self.set_entry_value('option_menu_behaviour', value)

    def set_num_steps(self, value):
        return self.set_entry_value('entry_num_steps', value)

    def set_orig_angle(self, value):
        return self.set_entry_value('entry_orig_angle', value)

    def set_num_sensors(self, value):
        return self.set_entry_value('entry_num_sensors', value)

    def set_range_angle(self, value):
        return self.set_entry_value('entry_range_angle', value)

    def set_robot_radio(self, value):
        return self.set_entry_value('entry_robot_radio', value)

    def set_robot_command(self, value):
        return self.set_entry_value('entry_robot_command', value)

    def set_robot_advance(self, value):
        return self.set_entry_value('entry_robot_advance', value)

    def set_d0(self, value):
        return self.set_entry_value('entry_d0', value)

    def set_eta(self, value):
        return self.set_entry_value('entry_eta', value)

    def set_d1(self, value):
        return self.set_entry_value('entry_d1', value)

    def set_epsilon(self, value):
        return self.set_entry_value('entry_epsilon', value)

    def set_delta(self, value):
        return self.set_entry_value('entry_delta', value)



