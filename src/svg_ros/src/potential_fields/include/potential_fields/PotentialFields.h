#ifndef _POTENTIAL_FIELDS_H
#define _POTENTIAL_FIELDS_H

#include <types/Coord.h>
#include <types/Inputs.h>
#include <types/AdvanceAngle.h>
#include <types/Raw.h>

class PotentialFields {
private:
    float d0, eta, d1, epsilon, delta;
    Coord lastPositions[2];
    Coord obstacles[4096];
    Coord obstacle;
    int indexObstacles;

public:
    PotentialFields(float, float, float, float, float);
    float mod(Coord v);
    Coord getAttractiveForce(Coord, Coord);
    Coord getRepulsiveForce(Coord, Coord);
    bool getObstaclePosition(int, float, float, float, Raw, Coord);
    Coord getTotalForce(Coord, Coord, int);
    Coord getNextPosition(Coord, Coord);
    AdvanceAngle getDisplacementVector(Coord, Coord);
    bool checkPreviousAngle(float, float);
    AdvanceAngle generateNextPosition(Coord, Coord, Inputs, Raw, int*);
};

#endif