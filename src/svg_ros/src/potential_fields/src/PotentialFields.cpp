#include <ros/console.h>
#include <potential_fields/PotentialFields.h>
#include <types/Constants.h>

PotentialFields::PotentialFields(float d0, float eta, float d1, float epsilon, float delta) {
    this->d0 = d0;
    this->eta = eta;
    this->d1 = d1;
    this->epsilon = epsilon;
    this->delta = delta;
    this->indexObstacles = 0;
}

AdvanceAngle PotentialFields::generateNextPosition(Coord robot, Coord destination, Inputs inputs, Raw observations,
        int *stflag) {
    Coord  total_force, next_position;
    AdvanceAngle displacement;
    bool flag_obstacle;

    flag_obstacle = getObstaclePosition(inputs.getNumberSensors(), inputs.getAngleSensor(), inputs.getRangeSensor(),
            inputs.getLargestSensorValue(), observations, robot);
    if (flag_obstacle) {
        obstacles[indexObstacles].setX(obstacle.getX());
        obstacles[indexObstacles].setY(obstacle.getY());
        indexObstacles ++;
    }
    total_force = getTotalForce(robot, destination, indexObstacles);
    next_position = getNextPosition(robot, total_force);
    ROS_DEBUG("[arbiter] Next position (%f, %f)", next_position.getX(), next_position.getY());
    displacement = getDisplacementVector(robot, next_position);
    if (displacement.getDistance() > inputs.getMagnitudeAdvance()) {
        displacement.setDistance(inputs.getMagnitudeAdvance());
    }
    if (checkPreviousAngle(lastPositions[1].getY(), robot.getY()) &&
        checkPreviousAngle(lastPositions[1].getX(), robot.getX())) {
        *stflag = 1;
        ROS_DEBUG("[Arbitro] Se activa bandera de maquina de estado");
    }
    lastPositions[0] = robot;
    lastPositions[1] = lastPositions[0];

    return displacement;
}


/**
 * get_obs_location
 * Calcula la posición del obstaculo más cercano
 * Toma el sensor con el valor más bajo para calcular la posición del obstáculo
 * @param numberSensors es el número de sensores
 * @param thetaSensor  es el angulo de inicio de los sensors (usualmente vale -(range_sensor/2))
 * @param rangeSensor es el angulo que abarcan todos los sensores (de -(range_sensor/2) a (range_sensor/2))
 * @param observations contiene las mediciones de cada sensor
 * @return obs coordenadas del obstáculo
 */
bool PotentialFields::getObstaclePosition(int numberSensors, float thetaSensor, float rangeSensor, float largestValue,
        Raw observations, Coord robot) {
    float angleSensor = 0;
    float angleStep = rangeSensor / (numberSensors - 1);
    int nearestSensorIndex = 0;
    float nearestSensorValue = Constants::LARGEST_DISTANCE_SENSORS;

    for (int i = 0; i < numberSensors; i++) {
        if (observations.getSensors(i) < nearestSensorValue) {
            nearestSensorValue = observations.getSensors(i);
            nearestSensorIndex = i;
        }
    }
    if (nearestSensorValue > (largestValue * 0.85)) { // 90% threshold
        return false;
    }
    angleSensor = thetaSensor + nearestSensorIndex * angleStep;
    obstacle.setX(robot.getX() + nearestSensorValue * cos(robot.getAngle() + angleSensor));
    obstacle.setY(robot.getY() + nearestSensorValue * sin(robot.getAngle() + angleSensor));
    if (obstacle.getX() > 0.95 || obstacle.getX() < 0.05 || obstacle.getY() > 0.95 || obstacle.getY() < 0.05) {
        ROS_DEBUG("[Location] Se discrimina valor (%f, %f)", obstacle.getX(), obstacle.getY());
        return false;
    }
    ROS_DEBUG("[get_obs_loc] Sensor %i, value: %f, obstacle: (%f, %f)\n", nearestSensorIndex, nearestSensorValue, obstacle.getX(), obstacle.getY());
    return true;
}


/**
 * get_total_force
 * Calcula la fuerza total para generar el movimiento
 * @param d
 * @param e Factor para modificar la magnitud de la fuerza de atracción
 * @param d0
 * @param eta Factor para modificar la magnitud de la fuerza de repulsión
 * @param coords Arreglo que contiene las coordenadas del robot, el obstáculo y el destino
 * @return Fuerza total
 */
Coord PotentialFields::getTotalForce(Coord robot, Coord dest, int numObstacles) {
    float moddiff = 0.0f, modtotf = 0.0f;
    Coord  attf, repf, totf;

    repf = Coord(0.0f, 0.0f, 0.0f);
    attf = getAttractiveForce(robot, dest);
    ROS_DEBUG("[Attractive] (%f, %f)", attf.getX(), attf.getY());
    for (int i = 0; i < numObstacles; i++) {
        Coord aux = getRepulsiveForce(robot, obstacles[i]);
        repf.setX(repf.getX() + aux.getX());
        repf.setY(repf.getY() + aux.getY());
    }
    ROS_DEBUG("[Repulsive] (%f, %f)", repf.getX(), repf.getY());
    totf = attf + repf;
    modtotf = mod(totf);
    totf.setX(totf.getX() / modtotf);
    totf.setY(totf.getY() / modtotf);
    ROS_DEBUG("[Total Force] (%f, %f)", totf.getX(), totf.getY());
    return totf;
}


/**
 * get_atractive_force
 * Calcula la fuerza atractiva del objetivo
 * @param d1
 * @param epsilon
 * @param robot Posición actual del robot
 * @param dest Posición a la cual se quiere llegar
 * @return Vector de dirección de la fuerza atractiva generada
 */
Coord PotentialFields::getAttractiveForce(Coord robot, Coord dest) {
    Coord diff = robot - dest;
    float moddiff = mod(diff);

    // parabolic field type
    Coord attf = Coord(epsilon * diff.getX(), epsilon * diff.getY(), 0.0);
    // conic field type
    if (moddiff > d1) {
        attf.setX(attf.getX() / moddiff);
        attf.setY(attf.getY() / moddiff);
    }
    //ROS_DEBUG("[Attractive force] (%f, %f) mod: %f", attf.xc, attf.yc, mod(attf));
    return attf;
}

/**
 * get_repulsive_force
 * Calcula la fuerza repulsiva de los obstáculos encontrados
 * @param d0
 * @param eta
 * @param robot Posición actual del robot
 * @param obs Posición del obstáculo
 * @return Vector de dirección de la fuerza repulsiva generada
 */
Coord PotentialFields::getRepulsiveForce(Coord robot, Coord obs) {
    float factor = 0.0f;
    float mod_diff;
    Coord diff;
    Coord repulsive = Coord(0.0f, 0.0f, 0.0f);

    diff = robot - obs;
    mod_diff = mod(diff);
    //ROS_DEBUG("[respulsive_force]mod_dif: %f d0:%f", mod_diff,d0);
    if (mod_diff <= d0) {
        factor = -eta * ((1/mod_diff) - (1/d0)) * (1/std::pow(mod_diff, 3));
        repulsive = Coord(factor * diff.getX(), factor * diff.getY(), 0.0);
    }
    //ROS_DEBUG("[Repulsive force] (%f, %f) mod: %f", repulsive.xc, repulsive.yc, mod(repulsive));
    return repulsive;
}


/**
 * Obtiene las coordenadas de la nueva posición objetivo
 * @param delta
 * @param robot Posición del robot
 * @param totf Fuerza total de los campos potenciales
 * @return Coordenadas del nuevo punto objetivo
 */
Coord PotentialFields::getNextPosition(Coord robot, Coord totf) {
    return Coord(robot.getX() - delta * totf.getX(), robot.getY() - delta * totf.getY(), 0.0);
}


/**
 * Calcula la distancia y el ángulo del siguiente movimiento
 * @param robot Posición del robot
 * @param next_pos Coordenadas del punto objetivo
 * @return Vector de desplazamiento
 */
AdvanceAngle PotentialFields::getDisplacementVector(Coord robot, Coord next_pos) {
    AdvanceAngle displacement;

    Coord diff = next_pos - robot;
    displacement.setDistance(mod(diff));
    displacement.setAngle(std::atan2(diff.getY(), diff.getX()) - robot.getAngle());

    return displacement;
}


/**
 * mod
 * Función que calcula el módulo de un vector
 * @param v vector al que se quiere calcular su módulo
 */
float PotentialFields::mod(Coord v) {
    Coord result = v*v;
    return std::sqrt(result.getX() + result.getY());
}


bool PotentialFields::checkPreviousAngle(float now, float previous) {
    float threshold = previous * .1;
    float li = previous - threshold;
    float ls = previous + threshold;
    return now > li && now < ls;
}
