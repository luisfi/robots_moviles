#include <state_machines/StateMachine.h>
#include <state_machines/StateMachineAction.h>
#include <types/AdvanceAngle.h>
#include <ros/console.h>

StateMachine::StateMachine(float distance, float angle) {
    this->distance = distance;
    this->angle = angle;
    this->type = StateMachineType::AVOIDANCE;
    this->state = 0;
}

StateMachine::StateMachine(float distance, float angle, StateMachineType type) {
    this->distance = distance;
    this->angle = angle;
    this->type = type;
    this->state = 0;
}

AdvanceAngle StateMachine::next(int obstacle) {
    next(obstacle, 0, 0);
}

AdvanceAngle StateMachine::next(int dest, int intensity) {
    next(0, dest, intensity);
}

AdvanceAngle StateMachine::next(int obstacle, int dest, int intensity) {
    AdvanceAngle output;
    switch (this->type) {
        case StateMachineType::AVOIDANCE:
            output = avoidance(obstacle);
            break;
        case StateMachineType::DESTINATION:
            output = destination(dest, intensity);
            break;
        case StateMachineType::AVOIDANCE_DESTINATION:
            output = avoidanceDestination(obstacle, dest, intensity);
            break;
        case StateMachineType::FOLLOW_WALL:
            output = followWall(obstacle);
            break;
        case StateMachineType::SURROUND_OBSTACLE:
            output = surroundObstacle(obstacle);
            break;
        default:
            break;
    }
    return output;
}

AdvanceAngle StateMachine::avoidance(int obstacle) {
    AdvanceAngle gen_vector;
    switch (state) {
        case 0:
            if (obstacle == 0) {
                // There is not obstacle
                gen_vector = generateOutput(Action::FORWARD);
                this->state = 0;
            } else {
                gen_vector = generateOutput(Action::STOP);
                if (obstacle == 1) {
                    // Obtacle in the right
                    this->state = 1;
                } else if (obstacle == 2) {
                    // Obstacle in the left
                    this->state = 3;
                } else if (obstacle == 3) {
                    // Obstacle in the front
                    this->state = 5;
                }
            }
            break;
        case 1: // Backward, obstacle in the right
            gen_vector = generateOutput(Action::BACKWARD);
            this->state = 2;
            break;
        case 2: // Left turn
            gen_vector = generateOutput(Action::LEFT);
            this->state = 0;
            break;
        case 3: // Backward, obstacle in the left
            gen_vector = generateOutput(Action::BACKWARD);
            this->state = 4;
            break;
        case 4: // Right turn
            gen_vector = generateOutput(Action::RIGHT);
            this->state = 0;
            break;
        case 5: // Backward, obstacle in front
            gen_vector = generateOutput(Action::BACKWARD);
            this->state = 6;
            break;
        case 6: // Left turn
            gen_vector = generateOutput(Action::LEFT);
            this->state = 7;
            break;
        case 7: // Left turn
            gen_vector = generateOutput(Action::LEFT);
            this->state = 8;
            break;
        case 8: // Forward
            if (obstacle != 0) {
                gen_vector = generateOutput(Action::STOP);
                this->state = 0;
            } else {
                gen_vector = generateOutput(Action::FORWARD);
                this->state = 9;
            }
            break;
        case 9: // Forward
            if (obstacle != 0) {
                gen_vector = generateOutput(Action::STOP);
                this->state = 0;
            } else {
                gen_vector = generateOutput(Action::FORWARD);
                this->state = 10;
            }
            break;
        case 10: // Forward
            if (obstacle != 0) {
                gen_vector = generateOutput(Action::STOP);
                this->state = 0;
            } else {
                gen_vector = generateOutput(Action::FORWARD);
                this->state = 11;
            }
            break;
        case 11: // Forward
            if (obstacle != 0) {
                gen_vector = generateOutput(Action::STOP);
                this->state = 0;
            } else {
                gen_vector = generateOutput(Action::FORWARD);
                this->state = 12;
            }
            break;
        case 12: // Forward
            if (obstacle != 0) {
                gen_vector = generateOutput(Action::STOP);
                this->state = 0;
            } else {
                gen_vector = generateOutput(Action::FORWARD);
                this->state = 13;
            }
            break;
        case 13: // Forward
            if (obstacle != 0) {
                gen_vector = generateOutput(Action::STOP);
                this->state = 0;
            } else {
                gen_vector = generateOutput(Action::FORWARD);
                this->state = 14;
            }
            break;
        case 14: // Right turn
            gen_vector = generateOutput(Action::RIGHT);
            this->state = 15;
            break;
        case 15: // Right turn
            gen_vector = generateOutput(Action::RIGHT);
            this->state = 0;
            break;
        default:
            ROS_ERROR("[avoidance] State not defined.");
            gen_vector = generateOutput(Action::STOP);
            this->state = 0;
    }
    return gen_vector;
}

AdvanceAngle StateMachine::avoidanceDestination(int obstacle, int dest, int intensity) {
    AdvanceAngle gen_vector;
    switch (state) {
        case 0:
            if (intensity == 1) {
                gen_vector = generateOutput(Action::STOP);
                this->state = 1;
            } else {
                gen_vector = generateOutput(Action::FORWARD);
                this->state = 1;
            }
            break;
        case 1:
            if (obstacle == 0) {
                // there is not obstacle
                gen_vector = generateOutput(Action::FORWARD);
                this->state = 16;
            } else {
                gen_vector = generateOutput(Action::STOP);
                if (obstacle == 1) {
                    // Obtacle in the  right
                    this->state = 4;
                } else if (obstacle == 2) {
                    // Obtacle in the left
                    this->state = 2;
                } else if (obstacle == 3) {
                    // Obstacle in the front
                    this->state = 6;
                }
            }
            break;
        case 2: // Backward, obstacle in the left
            gen_vector = generateOutput(Action::BACKWARD);
            this->state = 3;
            break;
        case 3: // Right turn
            gen_vector = generateOutput(Action::RIGHT);
            this->state = 0;
            break;
        case 4: // Backward, obstacle in the right
            gen_vector = generateOutput(Action::BACKWARD);
            this->state = 5;
            break;
        case 5: // Left turn
            gen_vector = generateOutput(Action::LEFT);
            this->state = 0;
            break;
        case 6: // Backward, obstacle in front
            gen_vector = generateOutput(Action::BACKWARD);
            this->state = 7;
            break;
        case 7: // Left turn
            gen_vector = generateOutput(Action::LEFT);
            this->state = 8;
            break;
        case 8:// Left turn
            gen_vector = generateOutput(Action::LEFT);
            this->state = 9;
            break;
        case 9:// Left turn
            gen_vector = generateOutput(Action::LEFT);
            this->state = 10;
            break;
        case 10: // Forward
            gen_vector = generateOutput(Action::FORWARD);
            this->state = 11;
            break;
        case 11: // Forward
            gen_vector = generateOutput(Action::FORWARD);
            this->state = 12;
            break;
        case 12: // Forward
            gen_vector = generateOutput(Action::FORWARD);
            this->state = 13;
            break;
        case 13: // Right turn
            gen_vector = generateOutput(Action::RIGHT);
            this->state = 14;
            break;
        case 14: // Right turn
            gen_vector = generateOutput(Action::RIGHT);
            this->state = 15;
            break;
        case 15: // Right turn
            gen_vector = generateOutput(Action::RIGHT);
            this->state = 0;
            break;
        case 16: // check dest
            if (dest == 0) {
                // Go right
                gen_vector = generateOutput(Action::RIGHT);
                this->state = 3;
            } else if (dest == 1) {
                // Go left
                gen_vector = generateOutput(Action::LEFT);
                this->state = 5;
            } else if (dest == 2) {
                // Go right single
                gen_vector = generateOutput(Action::FORWARD);
                this->state = 3;
            } else if (dest == 3) {
                // Go left single
                gen_vector = generateOutput(Action::FORWARD);
                this->state = 5;
            }
            break;
        default:
            ROS_ERROR("[avoidanceDestination] State %d not defined.", state);
            gen_vector = generateOutput(Action::STOP);
            this->state = 0;
    }
    return gen_vector;
}

AdvanceAngle StateMachine::destination(int dest, int intensity) {
    AdvanceAngle gen_vector;
    switch (state) {
        case 0:
            if (intensity == 1) {
                gen_vector = generateOutput(Action::STOP);
                // 
                this->state = 0;
            } else {
                gen_vector = generateOutput(Action::RIGHT);
                // 
                this->state = 1;
            }
            break;
        case 1: // It checks for the dest
            if (dest == 0) {
                // Go right
                gen_vector = generateOutput(Action::RIGHT);
                this->state = 2;
            } else if (dest == 1) {
                // Go left
                gen_vector = generateOutput(Action::LEFT);
                this->state = 3;
            } else if (dest == 2) {
                // Go right single
                gen_vector = generateOutput(Action::RIGHT);
                this->state = 2;
            } else if (dest == 3) {
                // Go left single
                gen_vector = generateOutput(Action::RIGHT);
                this->state = 3;
            } else if (dest == 5) {
                // Go left single
                gen_vector = generateOutput(Action::RIGHT);
                this->state = 0;
            }
            break;
        case 2: // Right turn
            gen_vector = generateOutput(Action::RIGHT);
            this->state = 0;
            break;
        case 3: // Left turn
            gen_vector = generateOutput(Action::LEFT);
            this->state = 0;
            break;
        default:
            ROS_ERROR("[Destination] State %d not defined.", state);
            gen_vector = generateOutput(Action::STOP);
            this->state = 0;
    }
    return gen_vector;
}

AdvanceAngle StateMachine::followWall(int obstacle) {
    AdvanceAngle gen_vector;
    switch (state) {
        case 0:
            gen_vector = generateOutput(Action::FORWARD);
            if (obstacle != 0)
                this->state = 1;
            else
                this->state = 0;
            break;
        case 1:
            gen_vector = generateOutput(Action::BACKWARD);
            this->state = 2;
            break;
        case 2:
            gen_vector = generateOutput(Action::RIGHT);
            this->state = 0;
            break;
        default:
            ROS_ERROR("[followWall] State %d not defined.", state);
            gen_vector = generateOutput(Action::STOP);
            this->state = 0;
    }
    return gen_vector;
}

AdvanceAngle StateMachine::surroundObstacle(int obstacle) {
    AdvanceAngle gen_vector;
    switch (state) {
        case 0:
            gen_vector = generateOutput(Action::FORWARD);
            if (obstacle == 3)
                this->state = 1;
            else
                this->state = 0;
            break;
        case 1:
            gen_vector = generateOutput(Action::LEFT);
            if ((obstacle & 2) == 2)
                this->state = 1;
            else
                this->state = 2;
            break;
        case 2:
            gen_vector = generateOutput(Action::FORWARD);
            if ((obstacle & 1) == 0)
                this->state = 3;
            else if (obstacle == 1)
                this->state = 2;
            else
                this->state = 1;
            break;
        case 3:
            gen_vector = generateOutput(Action::RIGHT);
            this->state = 4;
            break;
        case 4:
            gen_vector = generateOutput(Action::FORWARD);
            if ((obstacle & 1) == 1)
                this->state = 2;
            else
                this->state = 3;
            break;
        default:
            ROS_ERROR("[surroundObstacle] State %d not defined.", state);
            gen_vector = generateOutput(Action::STOP);
            this->state = 0;
    }
    return gen_vector;
}

AdvanceAngle StateMachine::generateOutput(Action s) {
    AdvanceAngle output;
    switch (s) {
        case Action::STOP: // Stop
            output.setDistance(0.0f);
            output.setAngle(0.0f);
            break;
        case Action::FORWARD: // Forward
            output.setDistance(distance);
            output.setAngle(0.0f);
            break;
        case Action::BACKWARD: // Backward
            output.setDistance(-distance);
            output.setAngle(0.0f);
            break;
        case Action::LEFT:// Turn left
            output.setDistance(0.0f);
            output.setAngle(angle);
            break;
        case Action::RIGHT: // Turn right
            output.setDistance(0.0f);
            output.setAngle(-angle);
            break;
        default:
            ROS_ERROR("[StateMachine] Action not defined.");
            output.setDistance(0.0f);
            output.setAngle(0.0f);
            break;
    }
    return output;
}

StateMachineType StateMachine::getType() const {
    return type;
}

void StateMachine::setType(StateMachineType type) {
    StateMachine::type = type;
}
