#ifndef _STATE_MACHINE_H
#define _STATE_MACHINE_H

#include <state_machines/StateMachineType.h>
#include <state_machines/StateMachineAction.h>
#include <types/AdvanceAngle.h>

class StateMachine {
public:
    StateMachine(float, float);
    StateMachine(float, float, StateMachineType);
    AdvanceAngle next(int obstacle);
    AdvanceAngle next(int destination, int intensity);
    AdvanceAngle next(int obstacle, int destination, int intensity);
    StateMachineType getType() const;
    void setType(StateMachineType type);

private:
    float distance, angle;
    StateMachineType type;
    int state;

    AdvanceAngle generateOutput(Action);
    AdvanceAngle avoidance(int);
    AdvanceAngle destination(int, int);
    AdvanceAngle avoidanceDestination(int, int, int);
    AdvanceAngle followWall(int);
    AdvanceAngle surroundObstacle(int);
};

#endif