#ifndef _STATE_MACHINE_ACTION_H
#define _STATE_MACHINE_ACTION_H

enum class Action {
    FORWARD,
    BACKWARD,
    STOP,
    LEFT,
    RIGHT
};

#endif