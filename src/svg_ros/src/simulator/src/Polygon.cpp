#include <Polygon.h>

Polygon::Polygon() {
    this->name = "";
    this->type = "";
    this->numberVertex = 0;
}


const string &Polygon::getName() const {
    return name;
}

void Polygon::setName(const string &n) {
    Polygon::name = n;
}

const string &Polygon::getType() const {
    return type;
}

void Polygon::setType(const string &t) {
    Polygon::type = t;
}

int Polygon::getNumberVertex() const {
    return numberVertex;
}

void Polygon::setNumberVertex(int n) {
    this->numberVertex = n;
}

Vertex Polygon::getVertex(int index) {
    return vertex[index];
}

Line Polygon::getLine(int index) {
    return lines[index];
}

void Polygon::setVertex(Vertex v, int index) {
    this->vertex[index] = v;
}

void Polygon::setLine(Line l, int index) {
    this->lines[index] = l;
}