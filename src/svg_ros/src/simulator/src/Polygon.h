#ifndef _POLYGON_H
#define _POLYGON_H

#include <string>
#include <Vertex.h>
#include <Line.h>
#include <types/Constants.h>

using namespace std;

class Polygon {
private:
    string name, type;
    int numberVertex;
    Vertex vertex[Constants::NUM_MAX_VERTEX];
    Line lines[Constants::NUM_MAX_VERTEX];
public:
    Polygon();

    const string &getName() const;

    void setName(const string &name);

    const string &getType() const;

    void setType(const string &type);

    int getNumberVertex() const;

    void setNumberVertex(int numberVertex);

    Vertex getVertex(int);

    void setVertex(Vertex, int);

    Line getLine(int);

    void setLine(Line, int);

};


#endif