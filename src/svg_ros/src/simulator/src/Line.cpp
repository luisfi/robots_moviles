#include <Line.h>


Line::Line(float m, float b) {
    this->m = m;
    this->b = b;
}

Line::Line() {
    Line(0.0f, 0.0f);
}

float Line::getM() const {
    return m;
}

void Line::setM(float m) {
    Line::m = m;
}

float Line::getB() const {
    return b;
}

void Line::setB(float b) {
    Line::b = b;
}
