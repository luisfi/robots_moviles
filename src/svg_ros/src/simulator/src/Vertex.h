#ifndef _VERTEX_H
#define _VERTEX_H

class Vertex {
private:
    float x, y;
public:
    Vertex(float, float);
    Vertex();

    float getX() const;

    void setX(float x);

    float getY() const;

    void setY(float y);

};

#endif