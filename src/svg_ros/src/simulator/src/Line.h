#ifndef _LINE_H
#define _LINE_H

class Line {
private:
    float m, b;
public:
    Line(float, float);
    Line();

    float getM() const;

    void setM(float m);

    float getB() const;

    void setB(float b);
};

#endif