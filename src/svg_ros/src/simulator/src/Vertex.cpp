#include <Vertex.h>

Vertex::Vertex(float x, float y) {
    this->x = x;
    this->y = y;
}

Vertex::Vertex() {
    Vertex(0.0f, 0.0f);
}

float Vertex::getX() const {
    return x;
}

void Vertex::setX(float x) {
    Vertex::x = x;
}

float Vertex::getY() const {
    return y;
}

void Vertex::setY(float y) {
    Vertex::y = y;
}
