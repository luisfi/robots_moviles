#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <ros/ros.h>
#include <types/Coord.h>
#include <types/Constants.h>
#include <types/Raw.h>
#include <Vertex.h>
#include <Line.h>
#include <Polygon.h>

// Global variables
Polygon worldPolygons[Constants::MAX_NUM_POLYGONS], robotSensor;
int numberWorldPolygons;


/*
 * Calcula pendiente y ordenada al origen de cada elemento de los polígonos
 * y = m*x + b
 * m = (y2 - y1)/(x2 - x1)
 * b = (x2*y1 - y2*x1)/(x2 - x1)
 */
void findComponentsLines(int numberPolygons, Polygon *polygons) {
    int i, j;
    float m, b, x1, y1, x2, y2;

    for (i = 1; i < numberPolygons; i++) {
        for (j = 1; j < polygons[i].getNumberVertex(); j++) {
            x1 = polygons[i].getVertex(j).getX();
            x2 = polygons[i].getVertex(j + 1).getX() + .00001f; // It adds to avoid zero division
            y1 = polygons[i].getVertex(j).getY();
            y2 = polygons[i].getVertex(j + 1).getY() + .00001f;
            m = (y2 - y1) / (x2 - x1);
            b = (x2 * y1 - y2 * x1) / (x2 - x1);
            polygons[i].setLine(Line(m, b), j);
        }
        // Line of last and first vertex
        x1 = polygons[i].getVertex(j).getX();
        x2 = polygons[i].getVertex(1).getX() + .00001f;
        y1 = polygons[i].getVertex(j).getY();
        y2 = polygons[i].getVertex(1).getY() + .00001f;
        m = (y2 - y1) / (x2 - x1);
        b = (x2 * y1 - y2 * x1) / (x2 - x1);
        polygons[i].setLine(Line(m, b), j);
    }
}

/*
 * Verifica si los puntos (x,y) de intersección se encuentran sobre una linea
 */
int interval(float xIntersection, float yIntersection, Polygon polygon, int j) {
    float x1, x2, y1, y2;

    if (polygon.getVertex(j).getX() < polygon.getVertex(j + 1).getX()) {
        x1 = polygon.getVertex(j).getX();
        x2 = polygon.getVertex(j + 1).getX();
    } else {
        x2 = polygon.getVertex(j).getX();
        x1 = polygon.getVertex(j + 1).getX();
    }
    if (polygon.getVertex(j).getY() < polygon.getVertex(j + 1).getY()) {
        y1 = polygon.getVertex(j).getY();
        y2 = polygon.getVertex(j + 1).getY();
    } else {
        y2 = polygon.getVertex(j).getY();
        y1 = polygon.getVertex(j + 1).getY();
    }
    if (x1 != x2)
        if ((xIntersection < x1) || (xIntersection > x2))
            return 0;
    if (y1 != y2)
        if ((yIntersection < y1) || (yIntersection > y2))
            return 0;

    return 1;
}

/*
 * It finds if the intersection points are between the robot's line
 */
int intervalRobot(float xIntersection, float yIntersection, Polygon robot) {
    float x1, x2, y1, y2;

    if (robot.getVertex(1).getX() < robot.getVertex(2).getX()) {
        x1 = robot.getVertex(1).getX();
        x2 = robot.getVertex(2).getX();
    } else {
        x2 = robot.getVertex(1).getX();
        x1 = robot.getVertex(2).getX();
    }
    if (robot.getVertex(1).getY() < robot.getVertex(2).getY()) {
        y1 = robot.getVertex(1).getY();
        y2 = robot.getVertex(2).getY();
    } else {
        y2 = robot.getVertex(1).getY();
        y1 = robot.getVertex(2).getY();
    }
    if (x1 != x2)
        if ((xIntersection < x1) || (xIntersection > x2))
            return 0;
    if (y1 != y2)
        if ((yIntersection < y1) || (yIntersection > y2))
            return 0;

    return 1;
}

/*
 * It finds the distance between the robot and the intersection line
 */
float magnitud(Polygon robot, float xIntersection, float yIntersection) {
    float distance;
    float x, y;

    x = robot.getVertex(1).getX() - xIntersection;
    y = robot.getVertex(1).getY() - yIntersection;
    distance = sqrtf(x * x + y * y);

    return distance;
}

/*
 * It reads the file that contains the environment description
 */
int ReadPolygons(char *filename, Polygon *polygons) {
    FILE *filePolygons;
    char data[Constants::STRSIZ];
    float value;
    int numberVertex, numberPolygons = 1;
    bool finalized;

    filePolygons = fopen(filename, "r");
    if (filePolygons == NULL) {
        ROS_ERROR("File %s does not exists.", filename);
        exit(0);
    }
    while (fscanf(filePolygons, "%s", data) != EOF) {
        if ((strcmp("polygon", data) == 0)) {
            fscanf(filePolygons, "%s", data);
            polygons[numberPolygons].setType(data);
            fscanf(filePolygons, "%s", data);
            polygons[numberPolygons].setName(data);
            numberVertex = 1;
            finalized = false;
            while (!finalized) {
                fscanf(filePolygons, "%s", data);
                if (strcmp(")", data) == 0) {
                    polygons[numberPolygons].setNumberVertex(numberVertex - 1);
                    float x = polygons[numberPolygons].getVertex(1).getX();
                    float y = polygons[numberPolygons].getVertex(1).getY();
                    polygons[numberPolygons].setVertex(Vertex(x, y), numberVertex);
                    numberPolygons++;
                    finalized = true;
                } else {
                    sscanf(data, "%f", &value);
                    float x = value;
                    fscanf(filePolygons, "%s", data);
                    sscanf(data, "%f", &value);
                    float y = value;
                    polygons[numberPolygons].setVertex(Vertex(x, y), numberVertex);
                    numberVertex++;
                }
            }
        }
    }
    fclose(filePolygons);

    return numberPolygons;
}

/*
 * It finds the distance to the closest polygon
 */
float distanceClosestPolygon(int numberPolygons, Polygon *polygons, Polygon robot, int *indexSmallest) {
    float m, b, mRobot, bRobot;
    float smallestDistance = (float) (0x7fffffff);
    float xIntersection, yIntersection, distance;

    mRobot = robot.getLine(1).getM();
    bRobot = robot.getLine(1).getB();
    for (int i = 1; i < numberPolygons; i++) {
        for (int j = 1; j <= polygons[i].getNumberVertex(); j++) {
            m = polygons[i].getLine(j).getM();
            b = polygons[i].getLine(j).getB();
            xIntersection = (float) (b - bRobot) / (float) (mRobot - m);
            yIntersection = mRobot * xIntersection + bRobot;
            if (interval(xIntersection, yIntersection, polygons[i], j) == 1){
                if (intervalRobot(xIntersection, yIntersection, robot) == 1) {
                    distance = magnitud(robot, xIntersection, yIntersection);
                    if (distance < smallestDistance) {
                        smallestDistance = distance;
                        *indexSmallest = i;
                    }
                }
            }
        }
    }

    return smallestDistance;
}

/*
 * It reads the environment
 */
void readEnvironment(char *filename) {
    ROS_DEBUG("[simulation.h] Environment filename: %s", filename);
    numberWorldPolygons = ReadPolygons(filename, worldPolygons);
    findComponentsLines(numberWorldPolygons, worldPolygons);
}

/*
 * It returns a laser reading
 */
float getLaserValue(float x, float y, float angle) {
    int index = 0;
    float distance;
    float x2, y2, m, b;

    x2 = Constants::SIZE_LINE * cosf(angle) + x;
    y2 = Constants::SIZE_LINE * sinf(angle) + y;
    robotSensor.setVertex(Vertex(x, y), 1);
    robotSensor.setVertex(Vertex(x2, y2), 2);
    m = (y2 - y) / (x2 - x);
    b = (x2 * y - y2 * x) / (x2 - x);
    robotSensor.setLine(Line(m, b), 1);
    distance = distanceClosestPolygon(numberWorldPolygons, worldPolygons, robotSensor, &index);

    return distance;
}

/*
 * It generates simulated sensor values
 */
void
generateSensorValues(Coord robot, float startAngle, float range, Raw &sensorVector, int numberSensors, float largestValue) {
    float distance, angleIncrement;
    float x, y, theta;

    x = robot.getX();
    y = robot.getY();
    theta = robot.getAngle();
    if (range == 1) {
        angleIncrement = range;
    } else {
        angleIncrement = range / (numberSensors - 1);
    }
    sensorVector.setX(x);
    sensorVector.setY(y);
    for (int k = 0; k < numberSensors; k++) {
        float angle = theta + k * angleIncrement + startAngle;
        distance = getLaserValue(x, y, angle);
        if (distance > largestValue) {
            distance = largestValue;
        }
        sensorVector.setSensors(k, distance);
        sensorVector.setTheta(angle);
    }
}

// It checks if the robot new position is inside an obstacle
int checkNewPosition(float x1, float y1, float x2, float y2, float advanceDistance) {
    int index;
    float distance;
    float m, b;

    index = 0;
    x2 += 0.0001f; // To avoid zero division
    y2 += 0.0001f;
    robotSensor.setVertex(Vertex(x1, y1), 1);
    robotSensor.setVertex(Vertex(x2, y2), 2);
    m = (y2 - y1) / (x2 - x1);
    b = (x2 * y1 - y2 * x1) / (x2 - x1);
    robotSensor.setLine(Line(m, b), 1);
    distance = distanceClosestPolygon(numberWorldPolygons, worldPolygons, robotSensor, &index);
    if (distance > advanceDistance) {
        return 0;
    } else {
        return 1;
    }
}