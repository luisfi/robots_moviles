#include "types/AdvanceAngle.h"

AdvanceAngle::AdvanceAngle() {
	this->distance = 0.0;
	this->angle = 0.0;
}

AdvanceAngle::AdvanceAngle(float distance, float angle) {
	this->distance = distance;
	this->angle = angle;
}

float AdvanceAngle::getDistance() {
	return distance;
}

float AdvanceAngle::getAngle() {
	return angle;
}

void AdvanceAngle::setDistance(float distance) {
	this->distance = distance;
}

void AdvanceAngle::setAngle(float angle) {
	this->angle = angle;
}
