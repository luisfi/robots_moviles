#include <types/Behaviour.h>

Behaviour::Behaviour() {
  this->x = 0.0f;
  this->y = 0.0f;
  this->status = 0.0f;
  this->flagVector = 0;
}

Behaviour::Behaviour(float x, float y, float status, int flagVector) {
  this->x = x;
  this->y = y;
  this->status = status;
  this->flagVector = flagVector;
}

Behaviour::Behaviour(float x, float y, float status) {
  this->x = x;
  this->y = y;
  this->status = status;
  this->flagVector = 0;
}
Behaviour::Behaviour(float x, float y) {
  this->x = x;
  this->y = y;
  this->status = 0.0f;
  this->flagVector = 0;
}

float Behaviour::getX() {
  return this->x;
}

float Behaviour::getY() {
  return this->y;
}

float Behaviour::getStatus() {
  return this->status;
}

int Behaviour::getFlagVector() {
  return this->flagVector;
}

void Behaviour::setX(float x) {
  this->x = x;
}

void Behaviour::setY(float y) {
  this->y = y;
}

void Behaviour::setStatus(float status) {
  this->status = status;
}

void Behaviour::setFlagVector(int f) {
  this->flagVector = f;
}
