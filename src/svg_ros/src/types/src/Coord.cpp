#include "types/Coord.h"

Coord::Coord() {
	this->x = 0.0;
	this->y = 0.0;
	this->angle = 0.0;
}

Coord::Coord(float x, float y, float angle) {
	this->x = x;
	this->y = y;
	this->angle = angle;
}


 Coord Coord::operator+(const Coord& c) {
	Coord coord;
	coord.setX(this->x + c.x);
	coord.setY(this->y + c.y);
	coord.setAngle(this->angle + c.angle);
	return coord;
}


Coord Coord::operator-(const Coord& c) {
	Coord coord;
	coord.setX(this->x - c.x);
	coord.setY(this->y - c.y);
	coord.setAngle(this->angle - c.angle);
	return coord;
}

Coord Coord::operator*(const Coord& c) {
	Coord coord;
	coord.setX(this->x * c.x);
	coord.setY(this->y * c.y);
	coord.setAngle(this->angle * c.angle);
	return coord;
}

float Coord::getX() {
	return x;
}

float Coord::getY() {
	return y;
}

float Coord::getAngle() {
	return angle;
}

void Coord::setX(float x) {
	this->x = x;
}

void Coord::setY(float y) {
	this->y = y;
}

void Coord::setAngle(float angle) {
	this->angle = angle;
}
