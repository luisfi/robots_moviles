#include <types/Raw.h>
#include <ros/console.h>

Raw::Raw() {
    this->flag = 0;
    this->region = 0;
    this->x = 0.0f;
    this->y = 0.0f;
    this->theta = 0.0f;
    for (int i = 0; i < Constants::MAX_NUM_SENSORS; ++i) {
        this->sensors[i] = 0.0f;
    }
}


int Raw::getFlag() const {
    return flag;
}

void Raw::setFlag(int flag) {
    Raw::flag = flag;
}

int Raw::getRegion() const {
    return region;
}

void Raw::setRegion(int region) {
    Raw::region = region;
}

float Raw::getX() const {
    return x;
}

void Raw::setX(float x) {
    Raw::x = x;
}

float Raw::getY() const {
    return y;
}

void Raw::setY(float y) {
    Raw::y = y;
}

float Raw::getTheta() const {
    return theta;
}

void Raw::setTheta(float theta) {
    Raw::theta = theta;
}


float Raw::getSensors(int index) {
    return this->sensors[index];
}

void Raw::setSensors(int index, float value) {
    this->sensors[index] = value;
}

void Raw::printSensors() {
    for (int i = 0; i < Constants::MAX_NUM_SENSORS; ++i) {
        if (sensors[i] > 0)
            ROS_INFO("Debagger: %f", sensors[i]);
    }
}