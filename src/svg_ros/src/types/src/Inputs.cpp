#include "types/Inputs.h"

Inputs::Inputs() {
  this->xPositionOrigin = 0.0f;
  this->yPositionOrigin = 0.0f;
  this->xPositionTarget = 0.0f;
  this->yPositionTarget = 0.0f;
  this->anglePosition = 0.0f;
  this->angleSensor = 0.0f;
  this->rangeSensor = 0.0f;
  this->magnitudeAdvance = 0.0f;
  this->radiusRobot = 0.0f;
  this->maxAngle = 0.0f;
  this->largestSensorValue = 0.0f;
  this->d0 = 0.0f;
  this->eta = 0.0f;
  this->d1 = 0.0f;
  this->epsilon = 0.0f;
  this->delta = 0.0f;
  this->numberSensors= 0;
  this->numberSteps= 0;
  this->optionSelected = 0;
  this->activateNoise= 0;
  this->pathToFiles = string("");
  this->fileEnvironment = string("");
  this->sensorName = string("");
}

float Inputs::getXPositionOrigin() const {
    return xPositionOrigin;
}

void Inputs::setXPositionOrigin(float xPositionOrigin) {
    Inputs::xPositionOrigin = xPositionOrigin;
}

float Inputs::getYPositionOrigin() const {
    return yPositionOrigin;
}

void Inputs::setYPositionOrigin(float yPositionOrigin) {
    Inputs::yPositionOrigin = yPositionOrigin;
}

float Inputs::getXPositionTarget() const {
    return xPositionTarget;
}

void Inputs::setXPositionTarget(float xPositionTarget) {
    Inputs::xPositionTarget = xPositionTarget;
}

float Inputs::getYPositionTarget() const {
    return yPositionTarget;
}

void Inputs::setYPositionTarget(float yPositionTarget) {
    Inputs::yPositionTarget = yPositionTarget;
}

float Inputs::getAnglePosition() const {
    return anglePosition;
}

void Inputs::setAnglePosition(float anglePosition) {
    Inputs::anglePosition = anglePosition;
}

float Inputs::getAngleSensor() const {
    return angleSensor;
}

void Inputs::setAngleSensor(float angleSensor) {
    Inputs::angleSensor = angleSensor;
}

float Inputs::getRangeSensor() const {
    return rangeSensor;
}

void Inputs::setRangeSensor(float rangeSensor) {
    Inputs::rangeSensor = rangeSensor;
}

float Inputs::getMagnitudeAdvance() const {
    return magnitudeAdvance;
}

void Inputs::setMagnitudeAdvance(float magnitudEAdvance) {
    Inputs::magnitudeAdvance = magnitudEAdvance;
}

float Inputs::getRadiusRobot() const {
    return radiusRobot;
}

void Inputs::setRadiusRobot(float radioRobot) {
    Inputs::radiusRobot = radioRobot;
}

float Inputs::getMaxAngle() const {
    return maxAngle;
}

void Inputs::setMaxAngle(float maxAngle) {
    Inputs::maxAngle = maxAngle;
}

float Inputs::getLargestSensorValue() const {
    return largestSensorValue;
}

void Inputs::setLargestSensorValue(float largestSensorValue) {
    Inputs::largestSensorValue = largestSensorValue;
}

const string &Inputs::getPathToFiles() const {
    return pathToFiles;
}

void Inputs::setPathToFiles(const string &pathToFiles) {
    Inputs::pathToFiles = pathToFiles;
}

const string &Inputs::getFileEnvironment() const {
    return fileEnvironment;
}

void Inputs::setFileEnvironment(const string &fileEnvironment) {
    Inputs::fileEnvironment = fileEnvironment;
}

const string &Inputs::getSensorName() const {
    return sensorName;
}

void Inputs::setSensorName(const string &sensorName) {
    Inputs::sensorName = sensorName;
}

int Inputs::getNumberSensors() const {
    return numberSensors;
}

void Inputs::setNumberSensors(int numberSensors) {
    Inputs::numberSensors = numberSensors;
}

int Inputs::getNumberSteps() const {
    return numberSteps;
}

void Inputs::setNumberSteps(int numberSteps) {
    Inputs::numberSteps = numberSteps;
}

int Inputs::getOptionSelected() const {
    return optionSelected;
}

void Inputs::setOptionSelected(int optionSelected) {
    Inputs::optionSelected = optionSelected;
}

int Inputs::getActivateNoise() const {
    return activateNoise;
}

void Inputs::setActivateNoise(int activateNoise) {
    Inputs::activateNoise = activateNoise;
}

float Inputs::getD0() const {
    return d0;
}

void Inputs::setD0(float d0) {
    Inputs::d0 = d0;
}

float Inputs::getEta() const {
    return eta;
}

void Inputs::setEta(float eta) {
    Inputs::eta = eta;
}

float Inputs::getD1() const {
    return d1;
}

void Inputs::setD1(float d1) {
    Inputs::d1 = d1;
}

float Inputs::getEpsilon() const {
    return epsilon;
}

void Inputs::setEpsilon(float epsilon) {
    Inputs::epsilon = epsilon;
}

float Inputs::getDelta() const {
    return delta;
}

void Inputs::setDelta(float delta) {
    Inputs::delta = delta;
}

int Inputs::getActivateGui() const {
    return activateGUI;
}

void Inputs::setActivateGui(int activateGui) {
    activateGUI = activateGui;
}
