#ifndef _ADVANCE_ANGLE_H
#define _ADVANCE_ANGLE_H

class AdvanceAngle {

private:
	float distance, angle;

public:
	AdvanceAngle();
	AdvanceAngle(float, float);
	float getDistance();
	float getAngle();
	void setDistance(float);
	void setAngle(float);
};

#endif
