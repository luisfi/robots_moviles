#ifndef _INPUTS_H
#define _INPUTS_H

#include <string>

using namespace std;

class Inputs {

public:
    Inputs();

    float getXPositionOrigin() const;

    void setXPositionOrigin(float xPositionOrigin);

    float getYPositionOrigin() const;

    void setYPositionOrigin(float yPositionOrigin);

    float getXPositionTarget() const;

    void setXPositionTarget(float xPositionTarget);

    float getYPositionTarget() const;

    void setYPositionTarget(float yPositionTarget);

    float getAnglePosition() const;

    void setAnglePosition(float anglePosition);

    float getAngleSensor() const;

    void setAngleSensor(float angleSensor);

    float getRangeSensor() const;

    void setRangeSensor(float rangeSensor);

    float getMagnitudeAdvance() const;

    void setMagnitudeAdvance(float magnitudEAdvance);

    float getRadiusRobot() const;

    void setRadiusRobot(float radioRobot);

    float getMaxAngle() const;

    void setMaxAngle(float maxAngle);

    float getLargestSensorValue() const;

    void setLargestSensorValue(float largestSensorValue);

    const string &getPathToFiles() const;

    void setPathToFiles(const string &pathToFiles);

    const string &getFileEnvironment() const;

    void setFileEnvironment(const string &fileEnvironment);

    const string &getSensorName() const;

    void setSensorName(const string &sensorName);

    int getNumberSensors() const;

    void setNumberSensors(int numberSensors);

    int getNumberSteps() const;

    void setNumberSteps(int numberSteps);

    int getOptionSelected() const;

    void setOptionSelected(int optionSelected);

    int getActivateNoise() const;

    void setActivateNoise(int activateNoise);

    float getD0() const;

    void setD0(float d0);

    float getEta() const;

    void setEta(float eta);

    float getD1() const;

    void setD1(float d1);

    float getEpsilon() const;

    void setEpsilon(float epsilon);

    float getDelta() const;

    void setDelta(float delta);

    int getActivateGui() const;

    void setActivateGui(int activateGui);

private:
    float xPositionOrigin, yPositionOrigin, xPositionTarget, yPositionTarget, anglePosition;
    float angleSensor, rangeSensor;
    float magnitudeAdvance, radiusRobot, maxAngle, largestSensorValue;
    string pathToFiles, fileEnvironment, sensorName;
    int numberSensors, numberSteps, optionSelected;
    int activateNoise, activateGUI;
    float d0, eta, d1, epsilon, delta;
};

#endif 
