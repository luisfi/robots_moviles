#ifndef _BEHAVIOUR_H
#define _BEHAVIOUR_H

class Behaviour {
private:
  float x, y, status;
  int flagVector;

public:
  Behaviour();
  Behaviour(float, float, float, int);
  Behaviour(float, float, float);
  Behaviour(float, float);
  float getX();
  float getY();
  float getStatus();
  int getFlagVector();
  void setX(float);
  void setY(float);
  void setStatus(float);
  void setFlagVector(int);
};

#endif
