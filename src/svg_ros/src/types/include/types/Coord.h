#ifndef _COORD_H
#define _COORD_H

class Coord {

private:
	float x, y, angle;
public:
	Coord();
	Coord(float, float, float);
	float getX();
	float getY();
	float getAngle();
	void setX(float);
	void setY(float);
	void setAngle(float);
	Coord operator-(const Coord& c);
	Coord operator+(const Coord& c);
	Coord operator*(const Coord& c);
};

#endif
