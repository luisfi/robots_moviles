#ifndef _CONSTANTS_H
#define _CONSTANTS_H

class Constants {
public:
    static const int LIMIT_SIMULATOR;
    static const float DEFAULT_MAGNITUDE_ADVANCE;
    static const float PI;
    static const float TURN_ANGLE;
    static const float GOAL_DISTANCE_REACHED;
    static const float SENSOR_THRESHOLD;
    static const float RADIUS_ROBOT;
    static const float LARGEST_DISTANCE_SENSORS;
    static const int  MAX_NUM_SENSORS;
    static const int  MAX_NUM_POLYGONS;
    static const int  NUM_MAX_VERTEX;
    static const int  STRSIZ;
    static const int  SIZE_LINE;

};

const int Constants::LIMIT_SIMULATOR = 150;
const float Constants::DEFAULT_MAGNITUDE_ADVANCE = 0.04f;
const float Constants::PI = 3.1415926535f;
const float Constants::TURN_ANGLE = PI/8.f;
const float Constants::GOAL_DISTANCE_REACHED = 1.5;
const float Constants::SENSOR_THRESHOLD = 1.5 * DEFAULT_MAGNITUDE_ADVANCE;
const float Constants::RADIUS_ROBOT = 0.03;
const float Constants::LARGEST_DISTANCE_SENSORS = 5.0; // 5.0 meters
const int  Constants::MAX_NUM_SENSORS = 1024;
const int  Constants::MAX_NUM_POLYGONS = 100;
const int  Constants::NUM_MAX_VERTEX = 10;
const int  Constants::STRSIZ = 300;
const int  Constants::SIZE_LINE = 10000;



#endif