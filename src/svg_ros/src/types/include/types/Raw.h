#ifndef _RAW_H
#define _RAW_H

#include <types/Constants.h>

class Raw {
private:
    int flag, region;
    float x, y, theta;
    float sensors[Constants::MAX_NUM_SENSORS];
public:
    Raw();

    int getFlag() const;

    void setFlag(int flag);

    int getRegion() const;

    void setRegion(int region);

    float getX() const;

    void setX(float x);

    float getY() const;

    void setY(float y);

    float getTheta() const;

    void setTheta(float theta);

    const float *getSensors() const;

    float getSensors(int);

    void setSensors(int, float);

    void printSensors();
};

#endif