#ifndef SVG_ROS_PARSERPLANNER_H
#define SVG_ROS_PARSERPLANNER_H

#include <string>
#include <vector>

using namespace std;

class ParserPlanner {
public:
    ParserPlanner();
    vector<string> getNextCommnad();
    void readCommands(string filename);
    vector<string> parseLine(string s);
    void printCommands();
    bool isEmpty();
private:
    string fileName;
    vector<string> commands;
};

#endif //SVG_ROS_PARSERPLANNER_H
