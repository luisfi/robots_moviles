

// It quantizes the destination this later
#include <types/Constants.h>

int quantize_destination(Coord robot, Coord dest) {
    int value = 0;
    float angle;
    Coord dif;
    float theta;

    theta = robot.getAngle();
    ROS_DEBUG("[ros_util.h] Pose (%f, %f, %f) Dest (%f, %f)", robot.getX(), robot.getY(),
              theta, dest.getX(), dest.getY());
    dif = dif_vectors(robot, dest);
    angle = get_angle(0, 0, 0, dif.getX(), dif.getY());
    //angle=get_angle(0,0,0,dest.xc,dest.yc);
    angle = angle - theta;
    if (angle < 0) {
        angle = 2 * Constants::PI + angle;
    }
    if (angle < Constants::PI / 2) {
        value = 3;
    } else if (angle < Constants::PI) {
        value = 1;
    } else if (angle < 3 * Constants::PI / 2) {
        value = 0;
    } else {
        value = 2;
    }

    return value;
}
