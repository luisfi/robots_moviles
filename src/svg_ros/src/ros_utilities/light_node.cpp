/*********************************
* Light Tower simulation node    *
*                                *
*                                *
*                                *
*                                *
*********************************/

#include <time.h>
#include <sys/time.h>
#include "ros/ros.h"
#include "../simulator/simulation.h"
#include "svg_ros/LightSrv.h"
#include "svg_ros/OverSrv.h"
#include "../ros_utilities/receiveInputs.h"
#include <types/Coord.h>
#include <types/Inputs.h>
#include <types/Constants.h>

//Global variables to handle inputs and flags
Inputs inputsT; // Save inputs from GUI
int flg_read = 1;
int flg_dest = 0;
int flg_ovr = 0;
const float K_GOAL = Constants::GOAL_DISTANCE_REACHED * Constants::DEFAULT_MAGNITUDE_ADVANCE;
const float K_INTENSITY = 1.0 * K_GOAL;


// Quantize value functions
float get_angle(float ang, float c, float d, float X, float Y) {
    float x, y;

    x = c - X;
    y = d - Y;
    if ((x == 0) && (y == 0)) {
        return (0);
    }
    if (fabs(x) < 0.0001) {
        return ((y < 0.0f) ? 3 * Constants::PI / 2 : Constants::PI / 2) - ang;
    }
    else {
        if (x >= 0.0f && y >= 0.0f) {
            return ((float) (atan(y / x) - ang));
        } else if (x < 0.0f && y >= 0.0f) {
            return ((float) (atan(y / x) + Constants::PI - ang));
        } else if (x < 0.0f && y < 0.0f) {
            return ((float) (atan(y / x) + Constants::PI - ang));
        } else {
            return ((float) (atan(y / x) + 2 * Constants::PI - ang));
        }
    }
}

float magnitude(Coord vector) {
	Coord result = vector*vector;
	return (float) sqrt(result.getX() + result.getY());
}


Coord dif_vectors(Coord v1, Coord v2) {
    return v1-v2;
}


int quantize_destination(Coord robot, Coord dest) {
    int value = 0;
    float angle;
    Coord dif;
    float theta;

    theta = robot.getAngle();
    dif = dif_vectors(robot, dest);
    angle = get_angle(0, 0, 0, dif.getX(), dif.getY());
    //angle=get_angle(0,0,0,Coord_dest.xc,Coord_dest.yc);
    //printf("angle %f\n",angle);
    angle = angle - theta;
    if (angle < 0) angle = 2 * Constants::PI + angle;
    //printf("angle %f\n",angle);
    if (angle < Constants::PI / 2) {
        value = 3;
    } else if (angle < Constants::PI) {
        value = 1;
    } else if (angle < 3 * Constants::PI / 2) {
        value = 0;
    } else {
        value = 2;
    }
    /* if ( angle < Constants::PI/4 || (angle > 7*Constants::PI/4  && angle < 2*Constants::PI) ) value=5;
    else if( angle < Constants::PI / 2) value = 3;
    else if ( angle < Constants::PI ) value = 1;
    else if( angle < 3*Constants::PI/2 ) value = 0;
    else value = 2; */

    return value;
}


int quantize_intensity(Coord robot, Coord dest) {
    int value = 0;
    Coord attraction_force;
    float mag;

    attraction_force = dif_vectors(robot, dest);
    mag = magnitude(attraction_force);
    if (mag > K_INTENSITY) {
        value = 0;
    } else {
        value = 1;
    }

    return value;
}

/*
 * Callback que verifica su el motion_planer esta terminado
 */
bool over(svg_ros::OverSrv::Request &req, svg_ros::OverSrv::Response &res) {
    flg_ovr = req.over_flg;
    if (flg_ovr) {
        // ROS_INFO("Light node: motion planner it's over");
        flg_read = 1;
        flg_dest = 0; // Check this later
    }
    req.over_flg = 0;
}

/*
 * Callback que envía las Coordenadas del destino y los valores cuantizados al motion_planner
 */
bool send_destination(svg_ros::LightSrv::Request &req, svg_ros::LightSrv::Response &res) {
    int a;
    int b;
    Coord robot, dest;

    if (flg_read) {
        ROS_INFO("Light waiting for destination");
        inputsT  = receiveInputs("receiveInputs");
        ROS_DEBUG("[Light tower] (%f, %f)", inputsT.getXPositionTarget(), inputsT.getYPositionTarget());
    }
    flg_read = 0;
    robot.setX(req.coord_x);
    robot.setY(req.coord_y);
    robot.setAngle(req.coord_ang);
    dest.setX(inputsT.getXPositionTarget());
    dest.setY(inputsT.getYPositionTarget());
    a = quantize_intensity(robot, dest);
    b = quantize_destination(robot, dest);
    res.quantized_attraction = b;
    res.quantized_intensity = a;

    return true;
}

/*
 * Inicia los servicios
 */
void light_func() {
    ros::NodeHandle n;
    std::string inputs_node_name = "receiveInputs";
    ros::ServiceServer service = n.advertiseService("send_destination", send_destination);
    ros::ServiceServer service1 = n.advertiseService("over_light", over);
    ROS_INFO("Light tower simulation node started");
    ros::spin();
}


// Main program
int main(int argc, char *argv[]) {
    ros::init(argc, argv, "light_module");
    light_func();
    return 0;
}
