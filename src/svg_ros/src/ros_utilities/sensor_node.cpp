/********************************************************
*      Sensor node		    			*
*                                			*
*      Using the simulator:	   			*
*  	rosrun svg_ros sensor_node -selection 1 	*
*                                			*
*      Using the real data:	   			*
*  	rosrun svg_ros sensor_node -selection 0 	*
*                                			*
*                         Jesus Savage   		*
*                         FI-UNAM, 2017			*
*                                			*
*********************************************************/

#include <time.h>
#include <sys/time.h>
#include <ros/ros.h>
#include <ros/console.h>
#include "../simulator/simulation.h"
#include "svg_ros/SensorSrv.h"
#include "svg_ros/OverSrv.h"
#include "svg_ros/GetLaserServ.h"
#include "svg_ros/OverSrv.h"
#include "../ros_utilities/receiveInputs.h"
#include "../utilities/random.h"
#include "../utilities/utilities.h"
#include <types/Coord.h>
#include <types/Inputs.h>
#include <types/Raw.h>


//Global Variables

const int MAX_NUM_CHARACTERS = 1500;
Inputs inputsT;
int flg_ENVIRONMENT = 1;
int flg_read = 1;
int ovr = 0;

/*
 * Callback que verifica si el motion_planer ha terminado
 */
bool over(svg_ros::OverSrv::Request &req, svg_ros::OverSrv::Response &res) {
    ovr = req.over_flg;
    if (ovr) {
        flg_ENVIRONMENT = 1;
        flg_read = 1;
    }
    req.over_flg = 0;
}


/*
 * Callback que lee el entorno y simula valores de los sensores;
 * actualiza el valor de acuerdo al mundo, las coordenadas y la orientación
 * del robot
 */
bool get_coords(svg_ros::SensorSrv::Request &req, svg_ros::SensorSrv::Response &res) {
    int j, flg_noise = 1;
    char world_file[250];
    Coord coord_robot;
    Raw observations;

    if (flg_read) {
        inputsT = receiveInputs("receiveInputs");
    }
    flg_read = 0;
    coord_robot.setX(req.xPosition);
    coord_robot.setY(req.yPosition);
    coord_robot.setAngle(req.anglePosition);
    if (flg_ENVIRONMENT) {
        // Read environment must be read once only
        sprintf(world_file, "%s%s.wrl", inputsT.getPathToFiles().c_str(), inputsT.getFileEnvironment().c_str());
        readEnvironment(world_file);
        flg_ENVIRONMENT = 0;
    }
    // Function in ~/catkin_ws/src/svg_ros/src/simulator/simulation.h
    generateSensorValues(coord_robot, inputsT.getAngleSensor(), inputsT.getRangeSensor(),
                         observations, inputsT.getNumberSensors(), inputsT.getLargestSensorValue());
    flg_noise = inputsT.getActivateNoise();
    if (flg_noise == 1) {
        addNoiseToSensorInputs(observations, inputsT.getNumberSensors());
    }
    res.flag = observations.getFlag();
    res.region = observations.getRegion();
    res.x = observations.getX();
    res.y = observations.getY();
    res.theta = observations.getTheta();
    res.numberSensors = inputsT.getNumberSensors();
    for (j = 0; j < inputsT.getNumberSensors(); j++) {
        res.sensors[j] = observations.getSensors(j);
    }

    return true;
}

/*
 * Se comunica con el sensor laser del robot
 */
int ROS_get_laser(int num_sensors, float range, float init_angle, float *sensors) {
    ros::NodeHandle n;
    ros::ServiceClient client = n.serviceClient<svg_ros::GetLaserServ>("get_laser");
    svg_ros::GetLaserServ srv;

    srv.request.numberSensors = num_sensors;
    srv.request.range = range;
    srv.request.initAngle = init_angle;
    if (! client.call(srv)) {
        ROS_ERROR("Failed to call service get_laser");
        printf("Failed to call service get_laser");
        return 1;
    }
    for (int i = 0; i < num_sensors; i++) {
        sensors[i] = srv.response.sensors[i];
        ROS_DEBUG("[sensor_node.cpp] Sensors[%d]: %f", i, srv.response.sensors[i]);
    }

    return 0;
}

/*
 * Callback que lee los datos del sensor
 */
bool get_laser(svg_ros::SensorSrv::Request &req, svg_ros::SensorSrv::Response &res) {
    float sensors[700];

    if (flg_read) {
        inputsT = receiveInputs("receiveInputs");
    }
    flg_read = 0;
    ROS_get_laser(inputsT.getNumberSensors(), inputsT.getRangeSensor(), inputsT.getAngleSensor(),
            sensors);
    for (int j = 0; j < inputsT.getNumberSensors(); j++) {
        res.sensors[j] = sensors[j];
    }
    res.flag = 1;
    res.region = 0;
    res.x = 0;
    res.y = 0;
    res.theta = 0;
    res.numberSensors = inputsT.getNumberSensors();

    return true;
}


// it selects simulated sensors
void sensor_func() {
    ros::NodeHandle n;
    ros::ServiceServer service = n.advertiseService("send_position", get_coords);
    printf("Using simulated laser data\n");
    ros::ServiceServer service1 = n.advertiseService("over_sensor", over);
    ROS_INFO("Sensors node started");
    ros::spin();
}


// It selects real sensors
void sensor_func_1() {
    ros::NodeHandle n;

    // Real
    ros::ServiceServer service = n.advertiseService("send_position", get_laser);
    // Simulated
    ros::ServiceServer service2 = n.advertiseService("send_position2", get_coords);
    ros::ServiceServer service1 = n.advertiseService("over_sensor", over);
    ROS_INFO("Sensors node started");
    ros::spin();
}


// Main program
int main(int argc, char *argv[]) {
    ros::init(argc, argv, "sensor_module");
    sensor_func_1();

    return 0;
}
