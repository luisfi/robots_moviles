//
// Created by rmai on 12/06/18.
//

#include "ParserPlanner.h"
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <fstream>
#include <string>
#include <vector>
#include <iostream>

using namespace std;

ParserPlanner::ParserPlanner() {
    this->fileName = "/home/rmai/catkin_ws/src/svg_ros/data/empty.raw";
}


void ParserPlanner::readCommands(string filename) {
    FILE *fp;
    char buffer[255];
    fp = fopen(filename.c_str(), "r");
    while(fgets(buffer, 255, fp)) {
        this->commands.insert(this->commands.begin(), string(buffer));
    }
    fclose(fp);
}

vector<string> ParserPlanner::parseLine(string l) {
    vector<string> results;
    boost::split(results, l, boost::is_any_of(" "));
    return results;
}


vector<string> ParserPlanner::getNextCommnad() {
    string line = this->commands.back();
    this->commands.pop_back();
    return parseLine(line);
}

void ParserPlanner::printCommands() {
    for (int i = 0; i < this->commands.size(); i++) {
        cout << this->commands[i];
    }
}


bool ParserPlanner::isEmpty() {
    return (this->commands.size() == 0);
}
