/********************************
*        Node to manage          *
*            inputs              *
*           from GUI             *
*         to other nodes         *
*                                *
*********************************/
#include <ros/ros.h>
#include <ros/console.h>
#include <svg_ros/InputsSrv.h>
#include <svg_ros/InputsPlannerSrv.h>
#include <svg_ros/OverSrv.h>
#include <svg_ros/ReadySrv.h>
#include <types/Inputs.h>
#include <types/Constants.h>


#define LARGEST_DISTANCE_SENSORS 5.0 // 5.0 meters

// Global variable to manage inputs
Inputs inputsT;
int flgGUI = 1;
int flgEND = 1;

int ready_gui() {
    static int flg = 1;
    static ros::NodeHandle n;
    static ros::ServiceClient client;
    svg_ros::ReadySrv srv;

    ROS_INFO("Ready GUI");
    if (flg == 1) {
        client = n.serviceClient<svg_ros::ReadySrv>("inputs_ready"); // connecting to service ReadySrv:inputs_ready
        flg = 0;
    }
    srv.request.ready = 1;
    if (client.call(srv)) {
        ROS_INFO("GUI data is available");
        srv.request.ready = 1;
    } else {
        ROS_ERROR("Failed to call service ReadySRV");
        return 1;
    }

    return 0;
}

/*
 * Inicializa las entradas en caso de que no haya entradas reales
 */
Inputs initialize() {
    Inputs i;
    i.setXPositionOrigin(.10f);
    i.setYPositionOrigin(.10f);
    i.setAnglePosition(0.0f);
    i.setXPositionTarget(0.750f);
    i.setYPositionTarget(1.60f);
    i.setSensorName("laser");
    i.setNumberSensors(2);
    i.setAngleSensor(-0.2f);
    i.setRangeSensor(0.4);
    i.setFileEnvironment("final");
    i.setPathToFiles("../data/");
    i.setRadiusRobot(Constants::RADIUS_ROBOT);
    i.setMagnitudeAdvance(Constants::DEFAULT_MAGNITUDE_ADVANCE);
    i.setMaxAngle(Constants::TURN_ANGLE);
    i.setNumberSteps(Constants::LIMIT_SIMULATOR);
    i.setOptionSelected(1);
    i.setLargestSensorValue(LARGEST_DISTANCE_SENSORS);
    i.setActivateNoise(1);
    i.setD0(0.15f);
    i.setEta(0.002f);
    i.setD1(0.5f);
    i.setEpsilon(30.f);
    i.setDelta(1.0f);
    i.setActivateGui(flgGUI);
    return i;
}

/*
 * Callback que recibe las entradas de la GUI
 */
bool get_data_GUI(svg_ros::InputsSrv::Request &req, svg_ros::InputsSrv::Response &res) {
    inputsT.setXPositionOrigin(req.xPositionOrigin);
    inputsT.setYPositionOrigin(req.yPositionOrigin);
    inputsT.setAnglePosition(req.anglePosition);
    inputsT.setXPositionTarget(req.xPositionTarget);
    inputsT.setYPositionTarget(req.yPositionTarget);
    inputsT.setSensorName(req.sensorName);
    inputsT.setNumberSensors(req.numberSensors);
    inputsT.setAngleSensor(req.angleSensor);
    inputsT.setRangeSensor(req.rangeSensor);
    inputsT.setFileEnvironment(req.fileEnvironment);
    inputsT.setPathToFiles(req.pathToFiles);
    inputsT.setRadiusRobot(req.radiusRobot);
    inputsT.setMagnitudeAdvance(req.magnitudeAdvance);
    inputsT.setMaxAngle(req.maxAngle);
    inputsT.setNumberSteps(req.numberSteps);
    inputsT.setOptionSelected(req.optionSelected);
    inputsT.setLargestSensorValue(req.largestSensorValue);
    inputsT.setActivateNoise(req.activateNoise);
    inputsT.setD0(req.d0);
    inputsT.setEta(req.eta);
    inputsT.setD1(req.d1);
    inputsT.setEpsilon(req.epsilon);
    inputsT.setDelta(req.delta);
    flgGUI = 0;
    return true;
}

/*
 * Callback que envía los datos a los nodos
 */
bool send_data_nodes(svg_ros::InputsPlannerSrv::Request &req, svg_ros::InputsPlannerSrv::Response &res) {
    res.xPositionOrigin = inputsT.getXPositionOrigin();
    res.yPositionOrigin = inputsT.getYPositionOrigin();
    res.anglePosition = inputsT.getAnglePosition();
    res.xPositionTarget = inputsT.getXPositionTarget();
    res.yPositionTarget = inputsT.getYPositionTarget();
    res.sensorName = inputsT.getSensorName();
    res.numberSensors = inputsT.getNumberSensors();
    res.angleSensor = inputsT.getAngleSensor();
    res.rangeSensor = inputsT.getRangeSensor();
    res.radiusRobot = inputsT.getRadiusRobot();
    res.magnitudeAdvance = inputsT.getMagnitudeAdvance();
    res.maxAngle = inputsT.getMaxAngle();
    res.numberSteps = inputsT.getNumberSteps();
    res.optionSelected = inputsT.getOptionSelected();
    res.largestSensorValue = inputsT.getLargestSensorValue();
    res.fileEnvironment = inputsT.getFileEnvironment();
    res.pathToFiles = inputsT.getPathToFiles();
    res.activateNoise = inputsT.getActivateNoise();
    res.d0 = inputsT.getD0();
    res.eta = inputsT.getEta();
    res.d1 = inputsT.getD1();
    res.epsilon = inputsT.getEpsilon();
    res.delta = inputsT.getDelta();
    res.activateGUI = flgGUI;

    //ROS_INFO("sending back inputs: [ %f %f ]", (float)res.origin_x, (float)res.origin_y);
    //send to nodes when GUI input has been send
    /*
    if(!flgGUI){
        ROS_INFO("sending back inputs: [ %f %f ]", (float)res.origin_x, (float)res.origin_y);
        ROS_INFO("sending back count %i",count);
        count ++;
        if(count <= 1){
            //change state to wait for inputs
            flgGUI = 1;
            count = 0;
        }
    }
    res.flgGUI = flgGUI;
    */
    return true;
}

/*
 * Callback que verifica si el motion_planner ha terminado
 */
bool over_inputs(svg_ros::OverSrv::Request &req, svg_ros::OverSrv::Response &res) {

    res.answer = 1; //flag to assure communication
    flgEND = req.over_flg;
    flgGUI = 1;
    //printf("Ready again to receive data from GUI\n");

    return true;
}

/*
 * Callback que confirma a la GUI que ha terminado
 */
bool confirm_end(svg_ros::OverSrv::Request &req, svg_ros::OverSrv::Response &res) {
    res.answer = flgEND;
    // Send to nodes when GUI input has been send
    if (!flgEND) {
        //change state to wait for state
        flgEND = 1;
    }

    return true;
}


int main(int argc, char **argv) {
    ros::init(argc, argv, "inputs_Server");
    inputsT = initialize();
    ros::NodeHandle n;

    // Dos servidores, uno para escuchar inputs del gui, otro para enviar inputs a los otros nodos
    ros::ServiceServer service = n.advertiseService("sendInputs", get_data_GUI);
    ros::ServiceServer service2 = n.advertiseService("receiveInputs", send_data_nodes);
    // Dos servidores, uno para verificar si ha terminado, otro para indicarselo a la GUI
    ros::ServiceServer service4 = n.advertiseService("over_inputs", over_inputs);
    //ros::ServiceServer service5 = n.advertiseService("confirm_end",confirm_end);
    ROS_INFO("Ready to manage inputs from the GUI");
    ros::spin();

    return 0;
}
