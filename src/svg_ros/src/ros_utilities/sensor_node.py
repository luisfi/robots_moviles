#!/usr/bin/env python
# -*- coding: utf-8 -*-

from svg_ros.srv import *
import rospy
import math
from sensor_msgs.msg import LaserScan


class sensor_service:

    def __init__(self):
        s = rospy.Service('get_laser', GetLaserServ, self.handle_sensor)
        service_1 = rospy.Service("over_sensor", OverSrv, self.handle_over);
        rospy.Subscriber('laserscan', LaserScan, self.update_value, queue_size=1)
        print('[INFO][sensor_node.py] Ready laser_module')
        rospy.spin()

    # Initializaction of Hokuyo
    num_points = 650
    PI = 3.1415926535
    range_laser = 240
    complete_range = range_laser * PI / 180
    K1 = num_points / complete_range
    # K1 = complete_range / num_points
    ranges = []

    def update_value(self, msg):
        print('[DEBUG][update_value][sensor_node.py]')
        self.ranges = msg.ranges
        print("[DEBUG][sensor_node.py] Ranges[0]: " + str(self.ranges[0]))
        for x in range(0, len(self.ranges)):
            print("[DEBUG][sensor_node.py] Val " + str(x) + ":" + str(self.ranges[x]))

    def handle_over(self, req):
        ovr = req.over_flg
        # print "Handle Over flg ",str(ovr)
        if (ovr):
            # ROS_INFO("base_node received over signal")
            flg_read = 1
            flg_ENVIRONMENT = 1
        req.over_flg = 0

    def handle_sensor(self, req):
        sensors = range(700)
        num_sensors = req.numberSensors
        ranges = req.range
        offset = self.complete_range / 2
        init_angle = offset + req.init_angle
        inc_angle = ranges / num_sensors
        theta = init_angle
        k = 1
        # It reads the laser data from Hokuyo
        laser = self.ranges
        for j in range(0, num_sensors):
            index = int(self.K1 * theta)
            print float(laser[index])
            sensors[j] = float(laser[index])
            if math.isnan(sensors[j]):
                sensors[j] = 0
            k = k + 1
            theta = k * inc_angle + init_angle
            print('[DEBUG][sensor_node.py] Laser: ', str(j), " = ", str(sensors[j]))
        return GetLaserServResponse(sensors)


if __name__ == '__main__':
    try:
        rospy.init_node('laser_module')
        n = sensor_service()
    except rospy.ROSInterruptException:
        print('[DEBUG] [sensor_node.py] No se pudo inciar el nodo laser_module')