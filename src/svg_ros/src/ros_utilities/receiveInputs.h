#include <ros/ros.h>
#include <ros/console.h>
#include <svg_ros/InputsPlannerSrv.h>
#include <cstdlib>
#include <types/Inputs.h>

Inputs receiveInputs(string s) {
    Inputs inputs;
    static bool flg_first_time = true;
    static ros::NodeHandle n;
    static ros::ServiceClient client;
    static svg_ros::InputsPlannerSrv srv;
    bool isReceived;

    if (flg_first_time) {
        client = n.serviceClient<svg_ros::InputsPlannerSrv>(s);
        flg_first_time = false;
    }
    do {
        isReceived = client.call(srv);
        if (isReceived) {
            // Cuando no hay entrada en la GUI continua en espera
            if (srv.response.activateGUI) {
                isReceived = false;
                usleep(500);
                continue;
            }
            inputs.setXPositionOrigin(srv.response.xPositionOrigin);
            inputs.setYPositionOrigin(srv.response.yPositionOrigin);
            inputs.setAnglePosition(srv.response.anglePosition);
            inputs.setXPositionTarget(srv.response.xPositionTarget);
            inputs.setYPositionTarget(srv.response.yPositionTarget);
            inputs.setSensorName(srv.response.sensorName);
            inputs.setNumberSensors(srv.response.numberSensors);
            inputs.setAngleSensor(srv.response.angleSensor);
            inputs.setRangeSensor(srv.response.rangeSensor);
            inputs.setFileEnvironment(srv.response.fileEnvironment);
            inputs.setPathToFiles(srv.response.pathToFiles);
            inputs.setRadiusRobot(srv.response.radiusRobot);
            inputs.setMagnitudeAdvance(srv.response.magnitudeAdvance);
            inputs.setMaxAngle(srv.response.maxAngle);
            inputs.setNumberSteps(srv.response.numberSteps);
            inputs.setOptionSelected(srv.response.optionSelected);
            inputs.setLargestSensorValue(srv.response.largestSensorValue);
            inputs.setActivateNoise(srv.response.activateNoise);
            inputs.setD0(srv.response.d0);
            inputs.setEta(srv.response.eta);
            inputs.setD1(srv.response.d1);
            inputs.setEpsilon(srv.response.epsilon);
            inputs.setDelta(srv.response.delta);
        } else {
            ROS_INFO("No se pudieron recibir los valores de la GUI.");
            ROS_INFO("Reintentando.");
            usleep(500);
        }
    } while (!isReceived);

    return inputs;
}