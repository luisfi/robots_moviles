;***********************************************************
;*                 Templates definition                    *
;***********************************************************

(deftemplate item
    (slot type)
    (slot name)
    (slot zone)
    (slot image)
    (slot attributes)
    (multislot pose)
    (slot belongs)
    (slot status))

(deftemplate human
    (slot name)
    (slot zone)
    (slot image)
    (slot attributes)
    (multislot pose)
    (slot carrying)
    (slot status))

(deftemplate robot
    (slot name)
    (slot zone)
    (slot image)
    (slot attributes)
    (multislot pose)
    (slot carrying)
    (slot status))

(deftemplate goal-room 
    (multislot room))

(deftemplate goal-object
    (slot object))

(deftemplate goal-move
    (slot object)
    (slot on-top-of))

(deftemplate on-top-of
    (slot upper)
    (slot lower))

(deftemplate move-object
    (slot object)
    (slot to))

(deftemplate pick
    (slot object))

;***********************************************************
;*         Environment initial state                       *
;***********************************************************

(deffacts Initial-state-objects-rooms-zones-actors
    ; Object
    (item (type Object) (name apple)   (zone corridor) (image apple)   (attributes pick) (pose 0.08 0.57 2.0))
    (item (type Object) (name sushi)   (zone corridor) (image sushi)   (attributes pick) (pose 0.08 0.57 1.0))
    (item (type Object) (name milk)    (zone corridor) (image milk)    (attributes pick) (pose 0.08 0.57 0.0))
    (item (type Object) (name soap)    (zone corridor) (image soap)    (attributes pick) (pose 0.07 0.52 0.0))
    (item (type Object) (name shampoo) (zone corridor) (image shampoo) (attributes pick) (pose 0.07 0.52 0.0))
    (item (type Object) (name book)    (zone deposit)  (image book)    (attributes pick) (pose 0.22 1.10 0.0))
    (item (type Object) (name hammer)  (zone deposit)  (image hammer)  (attributes pick) (pose 0.22 1.10 1.0))
    
    ; Rooms
    (item (type Room) (name deposit)    (pose 0.29 0.93 0.0))
    (item (type Room) (name kitchen)    (pose 0.68 1.10 0.0))
    (item (type Room) (name corridor)   (pose 0.39 0.58 0.0))
    (item (type Room) (name studio)     (pose 0.13 0.24 0.0))
    (item (type Room) (name bedroom)    (pose 0.40 0.28 0.0))
    (item (type Room) (name service)    (pose 0.67 0.29 0.0))
    
    ; Doors
    (item (type Door) (name fridgedoor) (zone kitchen)  (status closed) (belongs fridge) (pose 0.72 0.89 0.0))
    (item (type Door) (name entrance)   (zone corridor) (status closed) (belongs corridor_wall) (pose 0.6 0.57 0.0))
    
    ; Furniture
    (item (type Furniture) (name bedroom_table) (zone bedroom) (pose 0.38 0.16 0.0))
    (item (type Furniture) (name deposit_table) (zone deposit) (pose 0.19 1.13 0.0))
    (item (type Furniture) (name fridge)        (zone kitchen) (pose 0.65 0.82 0.0))
    (item (type Furniture) (name service_table) (zone service) (pose 0.63 0.17 0.0))
    
    ; Humans
    (human (name Mother) (zone bedroom) (pose 0.59 0.25 0.0))
    (human (name Father) (zone service) (pose 0.73 0.21 0.0))
    
    ; Robot
    (robot (name Justina) (zone studio) (pose 0.09 0.16 0.0))
    
    ; Mine xDD
    (goal-room (room nil))
    
    ; (on-top-of (upper nothing)  (lower apple))
    ; (on-top-of (upper apple)    (lower sushi))
    ; (on-top-of (upper sushi)    (lower milk))
    ; (on-top-of (upper milk)     (lower floor))
    ; (on-top-of (upper nothing)  (lower shampoo))
    ; (on-top-of (upper shampoo)  (lower floor))
    ; (on-top-of (upper nothing)  (lower soap))
    ; (on-top-of (upper soap)     (lower floor))
    
    
    (move-object (object hammer) (to Mother))
    (move-object (object milk) (to kitchen))
    (move-object (object apple) (to kitchen))
    (move-object (object sushi) (to kitchen))
    (move-object (object soap) (to service))
    (move-object (object shampoo) (to service))
    (move-object (object book) (to Father))
    ; (move-object (object apple) (to bedroom))
    
    )

(open "plan.txt" plan "w")

; ############ House Navigation Rules ############

(defrule goto-room "Go to a room (triggers the others rules)"
    (declare (salience 20))
    ?goal <- (goal-room (room ?goal-room&~corridor $?rest))
    ?robot <- (robot (zone ?robot-room&~corridor))
    (item (type Room) (name ?goal-room))
    (item (type Room) (name ?robot-room))
    =>
    (retract ?goal)
    (if (eq ?robot-room ?goal-room) then
        (printout t "Already in " ?goal-room crlf)
        (assert (goal-room (room ?rest)))
    else
        (printout t "Going to corridor" crlf)
        (assert (goal-room (room corridor ?goal-room ?rest)))))

(defrule goto-room-directly "From the corridor the robot can go to any room"
    (declare (salience 30))
    ?goal <- (goal-room (room ?goal-room&~corridor $?rest))
    ?robot <- (robot (zone corridor))
    (item (type Room) (name ?goal-room) (pose $?room-pose))
    =>
    (printout plan "goto " ?goal-room crlf)
    (printout plan "goto " ?room-pose crlf)
    ; (printout t "Opening the door of " ?goal-room crlf)
    (retract ?goal)
    ; (printout t "Closing the door of " ?goal-room crlf)
    (modify ?robot (zone ?goal-room) (pose ?room-pose))
    (printout t "Now on " ?goal-room crlf)
    (assert (goal-room (room ?rest))))

(defrule goto-corridor "Go to the corridor before going to other room"
    (declare (salience 10))
    ?goal <- (goal-room (room corridor $?rest))
    ?robot <- (robot (zone ?robot-room&~corridor))
    (item (type Room) (name ?robot-room))
    (item (type Room) (name corridor) (pose $?corridor-pose))
    =>
    ; (printout t "Opening the door of " ?robot-room crlf)
    (retract ?goal)
    (printout plan "goto " corridor crlf)
    (printout plan "goto " ?corridor-pose crlf)
    ; (printout t "Closing the door of " ?robot-room crlf)
    (modify ?robot (zone corridor) (pose ?corridor-pose))
    (printout t "Now on corridor" crlf)
    (assert (goal-room (room ?rest))))

; ############ Object finding rules ############

(defrule begin-search "Triggers the other rules"
    ?goal <- (move-object (object ?object-name) (to ?goal-room))
    ?robot <- (robot (carrying nil) (status nil))
    ?object <- (item (type Object) (name ?object-name) (attributes pick))
    (item (type Room) (name ?goal-room))
    =>
    (printout t "Searching " ?object-name crlf)
    (printout plan "find " ?object-name crlf)
    (assert (pick (object ?object-name))))

(defrule begin-search-human "Triggers the other rules"
    ?goal <- (move-object (object ?object-name) (to ?goal-human))
    ?robot <- (robot (carrying nil) (status nil))
    ?human <- (human (name ?goal-human) (zone ?goal-room))
    ?object <- (item (type Object) (name ?object-name) (attributes pick))
    (item (type Room) (name ?goal-room))
    =>
    (printout t "Searching " ?object-name crlf)
    (printout plan "find " ?object-name crlf)
    (assert (pick (object ?object-name))))


(defrule find-object "Going to the room where the object is"
    (declare (salience 10))
    ?goal <- (pick (object ?object-name))
    ?robot <- (robot (zone ?robot-room) (carrying nil) (status nil))
    ?object <- (item (type Object) (name ?object-name) (zone ?object-room))
    ?room-stack <- (goal-room (room $?rest))
    =>
    (retract ?room-stack)
    (printout t "Going to " ?object-room crlf)
    (assert (goal-room (room ?object-room ?rest)))
    (modify ?robot (status searching)))

(defrule goto-object "Once in the same room as the object, navigate to the object"
    (declare (salience 20))
    ?goal <- (pick (object ?object-name))
    ?robot <- (robot (zone ?robot-room) (status searching))
    ?object <- (item (type Object) (name ?object-name) (zone ?robot-room) (pose $?object-pose))
    =>
    (printout t "Going to " ?object-pose crlf)
    (printout plan "goto " ?object-pose crlf)
    (modify ?robot (pose ?object-pose) (status picking))
    (printout t ?object-name " found! :D" crlf)
    (assert (goal-move (object nothing)(on-top-of ?object-name))))

; ############ Object relocating rules ############

(defrule finish-search "Object found, go to the objective room" 
    ?goal <- (move-object (object ?object-name) (to ?goal-room))
    ?robot <- (robot (carrying ?object-name) (status carrying))
    (item (type Room) (name ?goal-room))
    ?room-stack <- (goal-room (room $?rest))
    =>
    (retract ?room-stack)
    (printout t "Going to " ?goal-room crlf)
    (assert (goal-room (room ?goal-room ?rest))))

(defrule finish-search-human "Object found, go to the objective room" 
    ?goal <- (move-object (object ?object-name) (to ?goal-human))
    ?robot <- (robot (carrying ?object-name) (status carrying))
    ?human <- (human (name ?goal-human) (zone ?goal-room))
    (item (type Room) (name ?goal-room))
    ?room-stack <- (goal-room (room $?rest))
    =>
    (retract ?room-stack)
    (printout t "Going to " ?goal-room crlf)
    (assert (goal-room (room ?goal-room ?rest))))

(defrule deposit-object "In the room where the object must be deposited"
    (declare (salience 30))
    ?goal <- (move-object (object ?object-name) (to ?robot-room))
    ?robot <- (robot (carrying ?object-name) (zone ?robot-room) (status carrying))
    ?object <- (item (type Object) (name ?object-name) (status picked-up))
    ?furniture <- (item (name ?furniture-name) (type Furniture) (zone ?robot-room) (pose $?furniture-pose))
    =>
    (printout t "Going to " ?furniture-pose crlf)
    (printout plan "goto " ?furniture-pose crlf)
    ; (assert (on-top-of (upper ?object-name) (lower ?furniture-name)))
    (if (eq ?furniture-pose (create$ 0.65 0.82 0.0)) then 
        (printout plan "goto (0.72 0.89 0.0)" crlf)
        (printout plan "open door " ?furniture-name crlf))
    (printout t "Leaving " ?object-name " in " ?furniture-name crlf)
    (printout plan "release " ?object-name crlf)
    (if (eq ?furniture-pose (create$ 0.65 0.82 0.0)) then (printout plan "close door " ?furniture-name crlf))
    (modify ?object (zone ?robot-room) (pose ?furniture-pose) (belongs ?furniture-name) (status nil))
    (modify ?robot (status nil) (carrying nil) (pose ?furniture-pose))
    (retract ?goal))

(defrule deposit-object-human "In the room where the object must be deposited"
    (declare (salience 30))
    ?goal <- (move-object (object ?object-name) (to ?human-name))
    ?robot <- (robot (carrying ?object-name) (zone ?robot-room) (status carrying))
    ?human <- (human (name ?human-name) (zone ?robot-room) (pose $?human-pose))
    ?object <- (item (type Object) (name ?object-name) (status picked-up))
    =>
    (printout t "Going to " ?human-pose crlf)
    (printout plan "goto " ?human-pose crlf)
    ; (assert (on-top-of (upper ?object-name) (lower ?human-name)))
    (printout t "Giving " ?object-name " to " ?human-name crlf)
    (printout plan "release " ?object-name crlf)
    (modify ?object (zone ?robot-room) (pose ?human-pose) (belongs ?human-name) (status nil))
    (modify ?robot (status nil) (carrying nil) (pose ?human-pose))
    (retract ?goal))





; ############ Object manipulation rules ############

(defrule pick-directly "Once where the object is, pick the object... if you can"
    ?goal <- (pick (object ?object-name))
    ?secondary-goal <- (goal-move (object nothing)(on-top-of ?object-name))
    ?robot <- (robot (status picking) (pose ?x ?y ?a))
    ?object <- (item (type Object) (name ?object-name) (pose ?x ?y ?b) (attributes pick))
    (on-top-of (upper nothing) (lower ?object-name))
    ?stack-1 <- (on-top-of (upper ?object-name)(lower ?object2-name))
    =>
    (retract ?goal ?secondary-goal ?stack-1)
    (assert (on-top-of (upper nothing)(lower ?object2-name)))
    (modify ?robot (carrying ?object-name) (status carrying))
    (printout plan "grasp " ?object-name crlf)
    (printout t ?object-name " picked up! :D" crlf)
    (modify ?object (status picked-up) (pose =(random 1 10) =(random 1 10) =(random 2 10) )))

; (defrule clear-object "Trigers other rules"
    ; ?goal <- (pick (object ?object-name))
    ; ?robot <- (robot (status picking) (pose ?x ?y ?a))
    ; ?object <- (item (type Object) (name ?object-name) (pose ?x ?y ?b) (attributes pick))
    ; (on-top-of (upper ~nothing) (lower ?object-name))
    ; =>
    ; (assert (goal-move (object nothing)(on-top-of ?object-name)))
    ; (printout t "Clearing " ?object-name crlf))

(defrule move-to-floor
    ?goal <- (goal-move (object ?object-name) (on-top-of floor))
    ?robot <- (robot (status picking) (pose ?x ?y ?a))
    ?object <- (item (type Object) (name ?object-name) (pose ?x ?y ?b) (attributes pick))
    (on-top-of (upper nothing) (lower ?object-name))
    ?stack <- (on-top-of (upper ?object-name) (lower ?object2-name))	
    =>
    (retract ?goal ?stack)
    (assert (on-top-of (upper ?object-name) (lower floor))
        	(on-top-of (upper nothing) (lower ?object2-name)))
    (printout plan "grasp " ?object-name crlf)
    (printout plan "release " ?object-name crlf)
    (printout t ?object-name " moved on top of floor" crlf))

(defrule clear-upper-block
    ?robot <- (robot (status picking) (pose ?x ?y ?a))
    ?object <- (item (type Object) (name ?object-name) (pose ?x ?y ?b) (attributes pick))
    (goal-move (object ?object-name))
    (on-top-of (upper ?object2-name) (lower ?object-name))
    (item (type Object) (name ?object2-name) (pose ?x ?y ?c) (attributes pick))
    =>
    (printout t "Clearing " ?object-name crlf)
    (assert (goal-move (object ?object2-name)(on-top-of floor))))

(defrule clear-lower-block
    ?robot <- (robot (status picking) (pose ?x ?y ?a))
    ?object <- (item (type Object) (name ?object-name) (pose ?x ?y ?b) (attributes pick))
    (goal-move (on-top-of ?object-name))
    (on-top-of (upper ?object2-name) (lower ?object-name))
    (item (type Object) (name ?object2-name) (pose ?x ?y ?c) (attributes pick))
    =>
    (printout t "Clearing " ?object-name crlf)
    (assert (goal-move (object ?object2-name)(on-top-of floor))))

; ############ Position inference rules ############

(defrule add-on-top-of-floor
    (declare (salience 50))
    ?robot <- (robot (pose ?x ?y ?r))
    (item (type Object) (name ?object-name) (pose ?x ?y ?z))
    (test (= ?z 0.0))
    =>
    (assert (on-top-of (upper ?object-name) (lower floor)))
    ; (printout t ?object-name " is on top of the floor" crlf)
 )

(defrule add-on-top-of
    (declare (salience 50))
    ?robot <- (robot (pose ?x ?y ?r))
    (item (type Object) (name ?object1-name) (pose ?x ?y ?a))
    (item (type Object) (name ?object2-name) (pose ?x ?y ?b))
    (test (= ?a (+ ?b 1.0)))
    =>
    (assert (on-top-of (upper ?object1-name) (lower ?object2-name)))
    ; (printout t ?object1-name " is on top of " ?object2-name  crlf)
    )

(defrule add-nothing-on-top-of
    (declare (salience 40))
    ?robot <- (robot (pose ?x ?y ?r))
    (item (type Object) (name ?object1-name) (pose ?x ?y ?a))
    (item (type Object) (name ?object2-name) (pose ?x ?y ?b))
    (test (= ?a (+ ?b 1.0)))
    (not (exists (on-top-of (lower ?object1-name))))
    =>
    (assert (on-top-of (upper nothing) (lower ?object1-name)))
    ; (printout t "Nothing is on top of " ?object1-name  crlf)
 )

(defrule add-nothing-on-top-of-lower-floor
    (declare (salience 30))
    ?robot <- (robot (pose ?x ?y ?r))
    (item (type Object) (name ?object1-name) (pose ?x ?y ?a))
    (test (= ?a 0.0))
    (not (exists (on-top-of (lower ?object1-name))))
    =>
    (assert (on-top-of (upper nothing) (lower ?object1-name)))
    ; (printout t "Nothing is on top of " ?object1-name  crlf)
 )

(defrule add-nothing-on-top-of-floor
    ?fact <- (on-top-of (upper nothing) (lower floor))
    =>
    (retract ?fact)
    ; (printout t "Nothing is on top of " ?object1-name  crlf)
 )


(reset)
(run)
(close plan)
(exit)
