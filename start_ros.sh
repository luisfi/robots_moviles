#!/bin/bash

# Script para iniciar el simulador y los nodos de ROS

# Se utiliza xterm para ejecutar los nodos de ROS
# Se elimina cualquier instancia de los nodos
# Cuidado de tener otros procesos corriendo en xterm
killall xterm

# Se compila el workspace de ROS
catkin_make install

# Se ejecuta el planeador de acciones
# Se debe tener instalado CLIPS
xterm -geometry -hold -e ". ~/catkin_ws/devel/setup.zsh && roscd svg_ros/src/data/ && clips -f planner.clp" &

# Se inician los nodos de ROS
xterm -hold -e ". ~/catkin_ws/devel/setup.zsh && roscd svg_ros/src/data/ && rosrun svg_ros inputs" &
xterm -hold -e ". ~/catkin_ws/devel/setup.zsh && roscd svg_ros/src/data/ && rosrun svg_ros light_node" & 
xterm -hold -e ". ~/catkin_ws/devel/setup.zsh && roscd svg_ros/src/data/ && rosrun svg_ros sensor_node" & 
xterm -hold -e ". ~/catkin_ws/devel/setup.zsh && roscd svg_ros/src/data/ && rosrun svg_ros base_node " &
xterm -hold -e ". ~/catkin_ws/devel/setup.zsh && roscd svg_ros/src/data/ && rosrun svg_ros mv_turtle.py"  &
sleep 1
xterm -geometry 80x10+3+3 -hold -e ". ~/catkin_ws/devel/setup.zsh && roscd svg_ros/src/data/ && rosrun svg_ros manager_gui.py" & 
sleep 1
xterm -geometry 100x40+560+555 -hold -e ". ~/catkin_ws/devel/setup.zsh && roscd svg_ros/src/data/ && rosrun svg_ros motion_planner_ROS " &
