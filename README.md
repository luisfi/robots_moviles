Robot Simulator
===============

Es un simulador para probar los algoritmos básicos, de Inteligencia Artificial,
para que los robots puedan navegar en su entorno, realizados para la asignatura 
de Robots Móviles de la Facultad de Ingeniería de la UNAM.


Está construido bajo la plataforma [ROS](https://www.ros.org) con apoyo de la 
herramienta [catkin](http://wiki.ros.org/catkin). 

También se puede utilizar con el robot [Turtlebot](https://www.turtlebot.com/).

## ¿Cómo utilizar?

Probado con Ubuntu 18 y ROS Melodic Morenia.

### Requisitos
  
* Distribución GNU/Linux que permita instalar ROS
* [ROS Melodic Morenia](http://wiki.ros.org/Distributions)
* Catkin 
* [zsh](https://www.zsh.org/)

### Ejecutar

> Se da por hecho conocimiento básico de ROS y la herramienta catkin.

1. Descargar o clonar este repositorio

2. Mover el contenido al workspace de catkin. Por defecto */home/user/catkin_ws*.

3. En una *terminal* cambiar de directorio al workspace 

  $ cd /home/user/catkin_ws 

  donde *user* es el nombre de usuario en el sistema GNU/Linux.

4. Ejecutar el archivo start_ros.sh
  
  Se utiliza *zsh* para ejecutar los nodos de ROS. Si se quiere utilizar con bash
  se debe editar el archivo start_ros.sh y cambiar en todas las líneas zsh 
  por bash.

## Notas importantes

* El archivo start_ros_.sh ejectua el comando para construir el proyecto.

* Se utiliza el  emulador de terminal xterm para ejecutar los nodos.

* Para poder finalizar todas la ventanas abiertas se puede ejecutar:
  
  $ killall xterm
  
  Se debe tener precaución con este comando ya que si se tienen 
  otras terminales con xterm también serán finalizadas.

* Para reiniciar el simulador completo se puede volver a ejecutar start__ros.sh.
  Éste cerrará las ventanas abiertas antes, como se indica en el punto anterior, y vuelve a 
  ejecutar los procesos.

* Para poder ejecutar los procesos, tiene que estar activo el core utilizando
el comando *roscore*.

